module.exports = {
  'LOG_TYPE': {
  	'ERR': 'ERR',
  	'ERR_ADD_CREDIT': 'ERR_ADD_CREDIT',
  	'ADD_CREDIT': 'ADD_CREDIT',
  	'TRANSFER_CREDIT': 'TRANSFER_CREDIT',
    'RECEIVE_CREDIT_MAILBOX': 'RECEIVE_CREDIT_MAILBOX',
  	'PAY_CREDIT': 'PAY_CREDIT',
  	'REDEEM_CREDIT': 'REDEEM_CREDIT',
  	'ADD_USER': 'ADD_USER',
  	'REMOVE_USER': 'REMOVE_USER',
    'EXTEND_CPM': 'EXTEND_CPM',
    'EXTEND_CPM_BY_CTV': 'EXTEND_CPM_BY_CTV',
    'UPGRADE_CPM': 'UPGRADE_CPM',
    'JOIN_TOURNAMENT': 'JOIN_TOURNAMENT',
    'PAY_CREDIT_FOR_TOURNAMENT': 'PAY_CREDIT_FOR_TOURNAMENT',
    'PAY_CREDIT_UPGRADE_CPM': 'PAY_CREDIT_UPGRADE_CPM',
    'UPGRADE_CTV': 'UPGRADE_CTV',
    'REFUND_CREDIT_USER': 'REFUND_CREDIT_USER',
    'ADD_CHALLENGE': 'ADD_CHALLENGE',
    'JOIN_CHALLENGE': 'JOIN_CHALLENGE',
    'JOIN_PREDICT': 'JOIN_PREDICT',
    'ORDER_STATUS': 'ORDER_STATUS'
  },
  'ORDER_STATUS': {
    'NEW': 'Mới',
    'CONFIRMED': 'Đã xác nhận',
    'DELIVERED': 'Đã gửi',
    'DONE': 'Đã hoàn thành',
    'CANCELLED': 'Đã hủy'
  }
}
