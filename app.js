var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var routes = require('./routes/index');
var cors = require('cors');
var io = require('socket.io');
var User = require('./models/user');
var Type = require('./models/type');
var _ = require('lodash');
var jwt = require('jsonwebtoken');
var CONFIG = require('./config');
var Agenda = require('agenda');

var app = express();
app.io = io();
app.io.set('origins', '*:*');

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.use('/image', express.static(__dirname + '/uploads'));
// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(cors());
// Make io accessible to our router

/*
 * Mongoose config
 */
mongoose.connect(CONFIG.DATABASE);
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));

require('./libs/agenda');

app.use(function(req,res,next){
  req.io = app.io;
  next();
});

app.use('/', require('./routes/index'));

// // Set socket.io listeners.
app.io.on('connection', (socket) => {
  socket.join('connected');
  socket.on('user', (user)  => {
    console.log(`connected: ${user._id}`);
    socket.join('user/' + user._id);

  });

  socket.on('disconnect', (socket) => {
    console.log(`a user: disconnected`);
  });
});


/*
 * catch 404 and forward to error handler
 */
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});

module.exports = app;
