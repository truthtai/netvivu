var request = require('superagent');
var CONFIG = require('../config');
var Sms = require('../models/sms');
var Promise = require('bluebird');
var _ = require('lodash');

module.exports = {

  SEND: function(phone, tmp) {
    var locale;
    return new Promise(function(resolve, reject) {
      Sms
        .findOne({ name: tmp })
        .exec()
        .then(function(res) {
          locale = res.locale;
          request
            .post('https://api.ringcaptcha.com/' + CONFIG.RINGCAPTCHA.appKey + '/code/sms')
            .type('form')
            .send({ api_key: CONFIG.RINGCAPTCHA.apiKey })
            .send({ phone: '+84' + phone })
            .send({ locale: locale })
            .end(function(err, resp) {
              if (err) { reject(err) }
              resolve(resp.body);
            })
        })
    })
  },

  UPDATE_SMS_GATEWAY: function(sms) {
    // console.log(sms);
    return new Promise(function(resolve, reject) {
      var data = {};
      _.forEach(sms, function(k) {
        data[k.locale] = k.msg;
      });

      request
        .put('https://api.ringcaptcha.com/apps/' + CONFIG.RINGCAPTCHA.appKey)
        .type('form')
        .send({ api_key: CONFIG.RINGCAPTCHA.apiKey })
        .send({ custom_message: JSON.stringify({ verification: data }) })
        .end(function(err, resp) {
          if (err) { reject(err) }
          resolve(resp.body);
        });

    })
  },

  STATUS: function() {
    return new Promise(function(resolve, reject) {

      request
        .get('https://api.ringcaptcha.com/metrics?api_key='+CONFIG.RINGCAPTCHA.apiKey+'&from=2016-02-01&to=2016-03-30') 
        .end(function(err, resp) {
          if (err) { reject(err) }
          resolve(resp.body);
        });
    })

  }
}
