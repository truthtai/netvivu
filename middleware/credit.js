var request = require('superagent');
var CONFIG = require('../config');
var Promise = require('bluebird');
var _ = require('lodash');

module.exports = {

  USE_CARD: function(data) {
    return new Promise(function(resolve, reject) {
      request
        .post(CONFIG.GATEWAY_CARD.USE_CARD)
        .send(data)
        .set('Accept', 'application/json')
        .end(function(err, resp) {
          if (err) { reject(err) }
          resolve(resp.body);
        })
    })
  },

  GET_TRANS_DETAIL: function(data) {
    return new Promise(function(resolve, reject) {
      request
        .post(CONFIG.GATEWAY_CARD.GET_TRANS_DETAIL)
        .send(data)
        .set('Accept', 'application/json')
        .end(function(err, resp) {
          if (err) { reject(err) }
          resolve(resp.body);
        })
    })
  },

}
