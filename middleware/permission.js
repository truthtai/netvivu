var Permission = require('../models/permission');
var Type = require('../models/type');

module.exports = function(permission) {
  return function(req, res, next) {
    if(req.user && req.user.status !== 'active') {
      return res.status(401).json({message: 'Tài khoản đã bị khóa. Vui lòng liên hệ quản trị để biết thêm chi tiết'})
    }
    Permission
      .findOne({ name: permission })
      .exec()
      .then(function(per) {
        if(!per) { return res.status(401).json({message: 'Unauthorized'}); }
        Type
          .findOne({ name: req.user.type, permission: per._id })
          .exec()
          .then(function(type) {
            if (!type) {
              return res.status(401).json({message: 'Unauthorized'});
            } else {
              next()
            }
          })

      })
  }
}
