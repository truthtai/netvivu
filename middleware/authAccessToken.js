var User = require('../models/user');
var jwt = require('jsonwebtoken');
var CONFIG = require('../config');

module.exports = function(req, res, next) {
  // check header or url parameters or post parameters for token
  var token;
  if (req.headers['authorization'] && req.headers['authorization'].split(' ')[0] === 'Bearer') {
    token = req.headers['authorization'].split(' ')[1];
  } else if (req.query.accessToken) {
    token = req.query.accessToken;
  }

  // decode token
  if (token) {

    // verifies secret and checks exp
    jwt.verify(token, CONFIG.SECRET, function(err, decoded) {
      if (err) {
        return res.status(401).json({
          message: 'Failed to authenticate token.'
        });
      } else {
        // if everything is good, save to request for use in other routes
        // User
        //   .findOne({
        //     _id: decoded._id
        //   })
        //   // .populate('type', 'name')
        //   .populate({
        //     path: 'type'
        //   })
        //   .exec(function(errx, user) {
        //     if (errx) {
        //       return res.status(401).json({
        //         message: errx
        //       });
        //     }
        //     req.user = user.toJSON;
        //     console.log(req.user);
        //     next();
        //   })
          // .then(function(user) {
          //   if (!user) {
          //     return res.status(401).json({
          //       message: 'USER NOT FOUND'
          //     });
          //   } else {
          //     req.user = user;
          //     console.log(req.user);
          //     next();
          //   }
          //
          // });
        req.user = decoded;
        next();
      }
    });
  } else {
    // if there is no token
    // return an error
    return res.status(404).json({
      message: 'No token provided.'
    });
  }
}
