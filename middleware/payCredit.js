var User = require('../models/user');
var LOG = require('./log');
var Promise = require('bluebird');

module.exports = {

  PAY: function(userId, payAmount, reason) {
    return (function() {
      Promise.coroutine(function* () {
        const user = yield User.findOne({_id: userId}).exec();
        if(!user) return res.status(404).json({ message: 'NOT FOUND THIS USER'});
        if(user && user.credit >= payAmount) {
          const paidCredit  = user.credit - payAmount;
          const _updateUser = yield User.findByIdAndUpdate(user._id, {credit: paidCredit}).exec();
        }
        LOG.ADD('PAY_CREDIT', user._id, reason);

      })()
    })()
  },

  ADD: function(userId, payAmount, reason) {
    return (function() {
      Promise.coroutine(function* () {
        const user = yield User.findOne({_id: userId}).exec();
        if(!user) return res.status(404).json({ message: 'NOT FOUND THIS USER'});

        const paidCredit  = user.credit + payAmount;
        const _updateUser = yield User.findByIdAndUpdate(user._id, {credit: paidCredit}).exec();

        LOG.ADD('ADD_CREDIT', user._id, reason);

      })()
    })()
  }
}
