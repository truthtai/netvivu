var Log = require('../models/log');
var CONFIG = require('../config');
var CONSTANTS = require('../constants');

var Promise = require('bluebird');

module.exports = {

  ADD: function(type, userId, data) {
  	var info = {
  		name: CONSTANTS.LOG_TYPE[type],
  		userId: userId,
  		description: data
  	}
  	return new Promise(function(resolve, reject) {
	    Log.create(info, function(err, res) {
	    	if(err) { reject(err); }
	    	resolve(res);
	    })
	  })
  },
}
