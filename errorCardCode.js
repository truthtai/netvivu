module.exports = {
  'ERR_CODE': {
    '00': 'Mã số nạp tiền không tồn tại hoặc đã được sử dụng',
    '03': 'Thẻ đã được sử dụng',
    '04': 'Thẻ đã bị khóa',
    '05': 'Thẻ đã hết hạn sử dụng',
    '06': 'Thẻ chưa được kích hoạt',
    '07': 'Thực hiện sai quá số lần cho phép',
    '08': 'Giao dịch nghi vấn (Timeout từ Đơn vị phát hành thẻ, chưa xử lý xong)',
    '09': 'Sai định dạng thông tin truyền vào',
    '10': 'Partner không tồn tại',
    '11': 'Partner bị khóa',
    '13': 'Hệ thống của Đơn vị phát hà̀nh thẻ đang bận',
    '14': 'Sai password',
    '15': 'Sai địa chỉ IP',
    '16': 'Sai chữ ký',
    '20': 'Sai độ dà̀i mã số nạp tiề̀n',
    '21': 'Mã giao dịch không hợp lệ (> 0 và < 30 ký tự)',
    '23': 'Serial thẻ không hợp lệ',
    '24': 'Mã số nạp tiền và serial không khớp',
    '25': 'Trù̀ng mã giao dịch (transRef)',
    '26': 'Mã giao dịch không tồn tại',
    '28': 'Mã số nạp tiền không đúng định dạng (chỉ bao gồm ký tự số)',
    '40': 'Lỗi kết nối tới Đơn vị phát hành (Voucher Services ko kết nối được đến hệ thống đơn vị phát hành thẻ, ví dụ: mất kết nối)',
    '41': 'Lỗi khi Đơn vị phát hành thẻ xử lý giao dịch (Lỗi phát sinh khi đơn vị phát hành thẻ đang xử lý giao dịch)',
    '51': 'Đơn vị phát hà̀nh thẻ không tồn tại',
    '52': 'Đơn vị phát hà̀nh thẻ không hỗ trợ nghiệp vụ nà̀y',
    '99': 'Lỗi không xác định khi xử lý giao dịch'
  }
};

