var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;

var GiftSchema  = new Schema({
    name: { type: String, required: true },
    description: { type: String, required: true },
    credit: { type: Number, required: true },
    price: { type: Number, required: true },
    image: { type: String, required: true },
    status:{ type: Boolean, required: true },
    ctime: { type: Date, default: Date.now },
    utime: { type: Date, default: Date.now },
    category: { type: String, required: true },
    subCategory: { type: String, required: true }
});

module.exports = mongoose.model('Gift', GiftSchema);
