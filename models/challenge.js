var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;

var ChallengeSchema   = new Schema({
    name: { type: String, required: true },
    credit: { type: Number, required: true },
    password: { type: String, required: true, select: false},
    teamA: {
      phone: { type: String, required: true },
      userId: { type: Schema.Types.ObjectId, ref: 'User' },
      teams: { type: Array, default: '' },
      started: Boolean,
      locked: Boolean,
      isWin: Boolean
    },
    teamB: {
      phone: { type: String, default: '' },
      userId: { type: Schema.Types.ObjectId, ref: 'User' },
      teams: { type: Array, default: '' },
      started: Boolean,
      locked: Boolean,
      isWin: Boolean
    },
    isWin: { type: Schema.Types.ObjectId, ref: 'User' },
    ctime: { type: Date, default: Date.now },
    fee: { type: Number, required: true },
    countingTime: { type: Boolean, default: false },
    endTime: { type: Object },
    active: { type: Boolean, default: true }
});

module.exports = mongoose.model('Challenge', ChallengeSchema);
