var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;
// var Tournament = require('../models/tournament');

var PlayerSchema  = new Schema({
    // userId: { type: Schema.Types.ObjectId, ref: 'User' },
    name: { type: String, required: true },
    phone: { type:String, required: true },
    ownerId: { type: Schema.Types.ObjectId, ref: 'User' },
    tournamentId: { type: Schema.Types.ObjectId, ref: 'Tournament' },
    ctime: { type: Date, default: Date.now },
    isWin: { type: Boolean, default: false },
});

module.exports = mongoose.model('Player', PlayerSchema);
