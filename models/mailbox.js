var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var MailboxSchema = new Schema({
  title: { type: String, required: true },
  description: { type: String, required: true },
  sender: {
    id: { type: Schema.Types.ObjectId, ref: 'User' },
    action: {
      read: { type: Boolean },
      deleted: { type: Boolean },
      confirmCredit: { type: Boolean }
    },
    credit: { type: Number }
  },
  receiver: {
    id: { type: Schema.Types.ObjectId, ref: 'User' },
    action: {
      read: { type: Boolean },
      deleted: { type: Boolean },
      confirmCredit: { type: Boolean }
    },
    credit: { type: Number }
  },
  ctime: { type: Date, default: Date.now }
});

module.exports = mongoose.model('Mailbox', MailboxSchema);
