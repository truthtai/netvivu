var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;

var PermissionSchema   = new Schema({
    name: { type: String, required: true, unique: true },
    description: { type: String, required: true }
});

module.exports = mongoose.model('Permission', PermissionSchema);