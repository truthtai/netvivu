var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;

var OrderSchema  = new Schema({
    orderId: { type: String, required: true },
    products: [
      {
        pid: { type: Schema.Types.ObjectId, ref: 'Gift' },
        quantity: { type: Number },
        total: { type: Number }
      }
    ],
    customerId: { type: Schema.Types.ObjectId, ref: 'User' },
    total: { type: Number, required: true },
    status: { type: String, default: 'NEW' },
    ctime: { type: Date, default: Date.now },
    utime: { type: Date, default: Date.now },
    history: [
      {
        status: { type: String },
        note: { type: String },
        ctime: { type: Date }
      }
    ]
});

module.exports = mongoose.model('Order', OrderSchema);
