var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;
// var Tournament = require('../models/tournament');

var SmsSchema  = new Schema({
    // userId: { type: Schema.Types.ObjectId, ref: 'User' },
    name: { type: String },
    locale: { type: String },
    msg: { type: String },
});

module.exports = mongoose.model('Sms', SmsSchema);