var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;

var UserSchema   = new Schema({
    name: String,
    email: { type: String, unique: true },
    gender: String,
    accessToken: String,
    type: { type: Schema.Types.ObjectId, ref: 'Type' },
    phone: { type: String, unique: true },
    logIn: Boolean,
    credit: { type: Number, required: true },
    status: { type: String, required: true },
    utime: { type: Date, default: Date.now },
    ctime: { type: Date, default: Date.now },
    username: { type: String, unique: true, required: true },
    password: String,
    ownerId: { type: Schema.Types.ObjectId, ref: 'User' },
    willRemoveAt: { type: Date },
    expType: { type: Date },
    resetPasswordCode: String,
    extraInfo: {
      address: String,
      district: String,
      city: String,
      gpkd: String,
      bank: String,
      businessName: String,
      personID: String,
    },
    connected: {
      vltk: { type: Boolean, default: false },
    },
    loggedIP: [
      {
        ip: { type: String },
        ctime: { type: Date }
      }
    ]
});
var db = mongoose.model('User', UserSchema);
db.collection.ensureIndex({ username: 1 });
db.collection.createIndex({username: 'text', phone: 'text', email: 'text'}, function(error) {});
module.exports = db;
