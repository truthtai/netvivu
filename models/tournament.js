var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;
// var Player = require('../models/player');

var TournamentSchema  = new Schema({
    name: { type: String, required: true },
    startReg: { type: Date, required: true },
    endReg: { type: Date, required: true },
    startRun: { type: Date, required: true },
    endRun: { type: Date, required: true },
    remindTime: { type: Date },
    randomTime: { type: Date },
    description: String,
    credit: { type: Number, required: true },
    winner: Array,
    status: String,
    freeFirstTime: { type: Boolean, required: true },
    players : [{ type: Schema.Types.ObjectId, ref: 'Player' }],
    image: String,
    game: String
});

module.exports = mongoose.model('Tournament', TournamentSchema);
