var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;

var NewsSchema   = new Schema({
    name: { type: String, required: true },
    description: { type: String, required: true },
    ownerId: { type: Schema.Types.ObjectId, ref: 'User' },
    tags: [{ type: String, required: true }],
    slug: { type: String, required: true, unique: true },
    ctime: { type: Date, default: Date.now },
    utime: { type: Date, default: Date.now }
});

//mongoose.model('News', NewsSchema).collection.ensureIndex({_id: 'text', slug: 'text'}, function(error) {});
module.exports = mongoose.model('News', NewsSchema);
