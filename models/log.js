var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var LogSchema = new Schema({
  name: { type: String, required: true },
  description: { type: String, required: true },
  userId: { type: Schema.Types.ObjectId, ref: 'User' },
  ctime: { type: Date, default: Date.now },
});

module.exports = mongoose.model('Log', LogSchema);
