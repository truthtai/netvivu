var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;

var CategorySchema   = new Schema({
    name: String,
    slug: String,
    active: Boolean,
    index: Number
});

module.exports = mongoose.model('Category', CategorySchema);