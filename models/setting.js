var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;

var SettingSchema  = new Schema({
    name: { type: String, required: true },
    value: { type: String, required: true },
    ctime: { type: Date, default: Date.now },
});

module.exports = mongoose.model('Setting', SettingSchema);