var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;

var TypeSchema   = new Schema({
    name: { type: String, required: true, unique: true },
    permission: [{ type: Schema.Types.ObjectId, ref: 'Permission' }]
});

module.exports = mongoose.model('Type', TypeSchema);