var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;
// var Player = require('../models/player');

var PredictSchema  = new Schema({
    name: { type: String, required: true },
    startRun: { type: Date, required: true },
    endRun: { type: Date, required: true },
    credit: { type: Number, required: true },
    ctime: { type: Date, default: Date.now },
    isWin: { type: String },
    active: { type: Boolean, default: true },
    teams: {
      teamA: { type: String, required: true },
      teamB: { type: String, required: true }
    },
    players: {
      teamA: [
        { userId: { type: Schema.Types.ObjectId, ref: 'User' } }
      ],
      teamB: [
        { userId: { type: Schema.Types.ObjectId, ref: 'User' } }
      ]
    }
});

module.exports = mongoose.model('Predict', PredictSchema);
