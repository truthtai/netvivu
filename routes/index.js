var router = require('express').Router();

router.get('/', function(req, res, next) {
	res.json({uptime: new Date()})
});

router.use('/user', require('./user.js'));
// router.use('/category', require('./category.js'));
router.use('/type', require('./type.js'));
router.use('/permission', require('./permission.js'));
router.use('/tournament', require('./tournament.js'));
router.use('/sms', require('./sms.js'));
router.use('/news', require('./news.js'));
router.use('/logs', require('./log.js'));
router.use('/setting', require('./setting.js'));
router.use('/address', require('./address.js'));
router.use('/gift', require('./gift.js'));
router.use('/challenge', require('./challenge.js'));
router.use('/predict', require('./predict.js'));
router.use('/mailbox', require('./mailbox.js'));
router.use('/order', require('./order.js'));
router.use('/jx', require('./jx.js'));

module.exports = router;
