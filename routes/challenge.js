var router = require('express').Router();
var Log = require('../models/log');
var Challenge = require('../models/challenge');
var crypto = require('crypto');
var AUTH_TOKEN = require('../middleware/authAccessToken');
var PERMISSION = require('../middleware/permission');
var CONFIG = require('../config');
var PAY_CREDIT = require('../middleware/payCredit');
var LOG = require('../middleware/log');
var Promise = require('bluebird');
var User = require('../models/user');
var mongoose = require('mongoose');
var _ = require('lodash');

router.get('/', AUTH_TOKEN, PERMISSION('GET_ALL_CHALLENGE'), GET_ALL_CHALLENGE);
router.get('/:id', AUTH_TOKEN, PERMISSION('GET_CHALLENGE'), GET_CHALLENGE);
router.post('/:id/join', AUTH_TOKEN, PERMISSION('JOIN_CHALLENGE'), JOIN_CHALLENGE);
router.put('/:id/join/update', AUTH_TOKEN, JOIN_CHALLENGE_UPDATE);
router.get('/:id/join?stepTwo', AUTH_TOKEN, PERMISSION('JOIN_CHALLENGE'), JOIN_CHALLENGE_STEP_TWO);
router.post('/', AUTH_TOKEN, PERMISSION('ADD_CHALLENGE'), ADD_CHALLENGE);

module.exports = router;

function GET_ALL_CHALLENGE(req, res, next) {
  Challenge
    .find({ active: true })
    .sort({'ctime' : -1})
    .populate({
      path: 'teamA.userId',
      select: '_id username',
      models: 'User'
    })
    .exec()
    .then(challenges => {
      return res.status(200).json(challenges)
    })
}

function GET_CHALLENGE(req, res, next) {
  Challenge
    .findOne({ _id: req.params.id, active: true })
    .populate('userId', '_id name email')
    .exec()
    .then(function(c) {
      if (!c) {
        return res.status(404).json('Phòng thách đấu không tồn tại');
      } else {
        return res.status(200).json(logs)
      }
    })
}

function JOIN_CHALLENGE_UPDATE(req, res, next) {
  if (!req.body.teams.length || !req.body.teams.length < 5)  return res.status(400).json({message: 'Không đủ thành viên trong đội. Vui lòng bổ xung thêm'});
  Challenge.findByIdAndUpdate(req.params.id, {$set: {'teamB.teams': req.body.teams, 'teamB.phone': req.body.phone} } )
    .exec()
    .then(() => res.status(200).json({message: 'UPDATE SUCCESS'}))
    .catch(err => res.status(400).json({message: err}))
}

function JOIN_CHALLENGE(req, res, next) {

  if (!req.body.password) { return res.status(400).json('ERR');}
  const hash = crypto
    .createHmac("md5", CONFIG.SECRET)
    .update(req.body.password)
    .digest('hex');

  var teamB = {
    phone: '',
    userId: req.user._id,
    teams: [
      {name: ''}, {name: ''}, {name: ''}, {name: ''}, {name: ''}
    ]
  }

  Challenge.findOne({ _id: req.params.id, password: hash, active: true })
    .exec()
    .then(joinChallenge => {
      if(!joinChallenge){
        return res.status(404).json({message: 'Sai mã phòng'})
      } else {

      var challengeTeamA = String(joinChallenge.teamA.userId);
      var challengeTeamB = String(joinChallenge.teamB.userId);
      console.log(challengeTeamB);
      console.log(req.user._id);

      User.findOne({_id: req.user._id}).exec()
      .then((user) => {
        if(user) {
          user = user.toJSON();
          if(joinChallenge.isWin) {
            return res.status(401).json({message: 'Trận đấu đã có người thắng. Vui lòng tham gia trận khác'});
          }

          // if(user.credit < joinChallenge.credit) {
          //   return res.status(401).json({ message: 'Xu không đủ'});
          // };
          console.log('>>> +++++');
          console.log(challengeTeamA);
          console.log(user._id);
          if (challengeTeamA && challengeTeamA !== String(user._id)) {
            console.log('IS NOT TEAM A');
            console.log(challengeTeamB);

            console.log('>>> +++++ B');
            console.log(challengeTeamB === 'null');
            console.log(challengeTeamB);
            console.log(req.user._id);
            if (challengeTeamB === 'null') {
              console.log('not');

              if(user.credit < parseInt(joinChallenge.credit)) {
                console.log('>>>> not enough');
                return res.status(401).json({ message: 'Xu không đủ'});
              } else {
                Challenge.findByIdAndUpdate(req.params.id, { teamB }).then(() => {
                  console.log('>>>>> pay');
                  console.log(user._id);
                  console.log(parseInt(joinChallenge.credit));
                  PAY_CREDIT.PAY(user._id, parseInt(joinChallenge.credit), `Tạm ứng xu tham gia Thách đấu: ${joinChallenge.credit}`);
                  console.log('update ok');
                  LOG.ADD('JOIN_CHALLENGE', user._id, `Tham gia thách đấu thành công: ${joinChallenge.name} | ${joinChallenge.credit} | ${joinChallenge._id}`);
                  return res.status(200).json({message: joinChallenge})
                })
              }

            }
            if (challengeTeamB !== 'null' && challengeTeamB !== String(user._id)) {
              return res.status(404).json({message: 'Phòng đã đủ người'});
            }
            return res.status(200).json({message: joinChallenge})
          } else {
            return res.status(200).json({message: joinChallenge})
          }
        }

        });

      }
    })
    .catch(err => res.status(400).json(err));
}

function JOIN_CHALLENGE_STEP_TWO(req, res, next) {
  if (!req.body.password) { return res.status(400).json('ERR');}
  const hash = crypto
    .createHmac("md5", CONFIG.SECRET)
    .update(req.body.password)
    .digest('hex');
  const teamB = {
    phone: '',
    userId: req.user._id,
    teams: req.body.teams
  }

  Promise.coroutine(function* (){
    const challenge = yield Challenge.findOne({ _id: req.params.id }).exec()
    if(!challenge) return res.status(404).json({message: 'NOT FOUND'});

    const joinChallenge = yield Challenge.findOne({ _id: req.params.id, password: hash}).exec()
    if(!joinChallenge) return res.status(404).json({message: 'Mật khẩu vào phòng không đúng'});

    PAY_CREDIT.PAY(req.user._id, req.body.credit, `Tạm ứng xu tham gia Thách đấu: ${joinChallenge.credit}`);



  })().then()
      .catch()

  Challenge
    .findOne({ _id: req.params.id })
    .populate('userId', '_id name email')
    .exec()
    .then(function(c) {
      if (!c) {
        return res.status(404).json('NOT FOUND');
      } else {
        return res.status(200).json(logs)
      }
    })
}

function ADD_CHALLENGE(req, res, next) {
  var hash = crypto
    .createHmac("md5", CONFIG.SECRET)
    .update(req.body.password)
    .digest('hex');

  if(!req.body) { return res.status(400).json('ERR');}
  if(req.user.credit < req.body.credit) { return res.status(401).json({ message: 'Xu không đủ'}) }
  req.body.password = hash;
  req.body.teamA = {
    phone: req.body.phone,
    userId: req.user._id,
    teams: req.body.teams
  }

  req.body.teamB = {
    phone: '',
    teams: [
      {name: ''}, {name: ''}, {name: ''}, {name: ''}, {name: ''}
    ],
    userId: null
  }

  Challenge.create(req.body)
  .then((r) => {
    PAY_CREDIT.PAY(req.user._id, req.body.credit, `Tạm ứng xu Thách đấu: ${req.body.credit}`);
    // req.io.on('connection', (socket) => {
    //   socket.on('joinChallenge', (user)  => {
    //     console.log(`joinChallenge: ${user._id}`);
    //     socket.join('challengeRoom/' + r._id);
    //     req.io.sockets.to('challengeRoom/' + r._id).emit('msg', `${req.user.name} đã vào phòng`);
    //   });
    //   socket.on('disconnect', (socket) => {
    //     req.io.sockets.to('challengeRoom/' + r._id).emit('msg', `${req.user.name} đã thoát`);
    //   });
    // })
    LOG.ADD('ADD_CHALLENGE', req.user._id, `Tạo phòng thách đấu thành công: ${req.body.name} | ${req.body.credit} | ${r._id}`)
    return res.status(200).json({message: r});
  })
  .catch(err => res.status(400).json(err))
}
