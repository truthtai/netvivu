var router = require('express').Router();
var Predict = require('../models/predict');
var Player = require('../models/player');
var User = require('../models/user');
var Type = require('../models/type');
var crypto = require('crypto');
var moment = require('moment');
var _ = require('lodash');
var fs = require('fs');
var path = require('path');
var Log = require('../models/log');
var nodeExcel = require('excel-export');
var Promise = require('bluebird');
var slug = require('slug');
var PAY_CREDIT = require('../middleware/payCredit');
var AUTH_TOKEN = require('../middleware/authAccessToken');
var PERMISSION = require('../middleware/permission');
var SMS = require('../middleware/sms');
var LOG = require('../middleware/log');
var jwt = require('jsonwebtoken');
var CONFIG = require('../config');

router.get('/', AUTH_TOKEN, GET_ALL);
router.post('/:id/join', AUTH_TOKEN, PERMISSION('JOIN_PREDICT'), JOIN, extractInfo);
router.post('/:id/win', AUTH_TOKEN, PERMISSION('SET_WIN_PREDICT'), SET_WIN);
router.get('/:id', AUTH_TOKEN, GET);
router.delete('/:id', AUTH_TOKEN, PERMISSION('DELETE_PREDICT'), DELETE);
router.post('/', AUTH_TOKEN, PERMISSION('ADD_PREDICT'), ADD);
router.put('/:id', AUTH_TOKEN, PERMISSION('EDIT_PREDICT'), EDIT);

// router.get('/export/:id', AUTH_TOKEN, PERMISSION('EXPORT_PREDICT'), EXPORT);

module.exports = router;
function extractInfo(req, res, next) {

  User.findOne({_id: req.user._id})
  .exec()
  .then((user) => {
    var userJSON = user.toJSON();

    Type
      .findOne({
        _id: userJSON.type
      })
      .exec()
      .then(function(type) {
        userJSON.type = type.name;
        var userDataToken = _.omit(userJSON, ['password', 'loggedIP']);

        // create a token
        var token = jwt.sign(userDataToken, CONFIG.SECRET);
        req.io.in('user/' + userJSON._id).emit('userData', token);
        // return res.status(200).json({
        //   accessToken: token
        // });
        return res.status(200).json({message: 'Dự đoán thành công'});
      });

  })

    // if user is found and password is right
}

function GET_ALL(req, res, next) {
  var sort = {};

    if(req.query.sort === 'now' || req.query.sort === '') {
      sort = { startRun: { $lte: moment() }, endRun: { $gte: moment() }, isWin: null, active: true }
    }

    if(req.query.sort === 'timeout') {
      sort = { endRun: { $lte: moment() }, isWin: null }
    }

    if(req.query.sort === 'history') {
      sort = { isWin: {$ne:null} }
    }

    if(req.query.sort === 'all') {
      sort = {}
    }


  Predict
    .find(sort)
    .sort('-ctime')
    // .populate('players', '_id name ownerId isWin phone')
    .exec()
    .then(function(t) {
      if (!t) {
        return res.status(404).json('NOT FOUND');
      } else {
        return res.status(200).json(t)
      }
    })
}

function GET(req, res, next) {
  Predict
    .findOne({_id: req.params.id, active: true})
    .exec()
    .then(function(t) {
      if (!t) {
        return res.status(404).json({message: 'Trận đấu kông tồn tại'});
      } else {
        return res.status(200).json(t)
      }
    })
}

function ADD (req, res, next) {
  var currentDate = new Date();
  var predict = {
    name: req.body.name,
    startRun: moment.unix(req.body.startRun).format(),
    endRun: moment.unix(req.body.endRun).format(),
    credit: req.body.credit,
    teams: req.body.teams,
    players: {
      teamA: [],
      teamB: []
    },
    isWin: null
  }

  Predict
    .create(predict, function(err, resp) {
      if (err) return res.status(400).json(err);
      return res.status(200).json(resp);
    })
}

function JOIN (req, res, next) {
  if(!req.body.select) return res.status(401).json({message: 'Vui lòng chọn đội bạn dự đoán'});
  Predict.findOne({_id: req.params.id, active: true})
  .exec()
  .then(r => {
    if(!r) return res.status(404).json({message: 'Trận đấu không tồn tại'});
    if(r.credit > req.user.credit) return res.status(401).json({message: 'Không đủ xu tham gia'});
    if(r.isWin) return res.status(401).json({message: 'Trận đấu đã có kết quả dự đoán'});
    if(moment(r.endRun).unix() < moment().unix()) return res.status(400).json({message: 'Đã hết thời gian tham gia dự đoán'});

    var currentTeam;
    var currentTeamName;
    if(req.body.select === 'teamA') {
      currentTeamName = r.teams.teamA;
      currentTeam = { 'players.teamA': { userId: req.user._id } };
    } else {
      currentTeamName = r.teams.teamB;
      currentTeam = { 'players.teamB': { userId: req.user._id } };
    }

    Predict.findByIdAndUpdate(
        req.params.id,
        {$push: currentTeam },
        {safe: true, upsert: true, new : true},
        function(err, tour) {
          if (err) {
            return res.status(500).json({message: err});
          } else {
            PAY_CREDIT.PAY(req.user._id, parseInt(r.credit), `Tạm ứng xu tham gia dự đoán: ${r.credit} | ${r.name}`);
            LOG.ADD('JOIN_PREDICT', req.user._id, `Tham gia dự đoán thành công. Trận đấu dự đoán: ${r.name} (${r.credit}) | Đội dự đoán sẽ thắng: ${currentTeamName}`);
            Predict.find({ startRun: { $lte: moment() }, endRun: { $gte: moment() }, isWin: null, active: true })
            .sort('-ctime')
            .exec()
            .then(r => {
              req.io.emit('predictNow', r);
              next();
            });
          }
        }
    );
  })
  .catch(err => {
    console.log(err);
  })
}

function EDIT (req, res, next) {
  var currentDate = new Date();
  var tournament = {
    name: req.body.name,
    startRun: moment.unix(req.body.startRun).format(),
    endRun: moment.unix(req.body.endRun).format(),
    credit: req.body.credit,
    ctime: currentDate,
    teams: req.body.teams,
  }

  Predict
    .update({_id:req.params.id}, tournament, function(err, resp) {
      if (err) return res.status(400).json(err);
      Predict.find({ endRun: { $lte: moment() }, isWin: null })
      .sort('-ctime')
      .exec()
      .then(rx => {
        req.io.emit('predictTimeout', rx);
      });

      Predict.find({ startRun: { $lte: moment() }, endRun: { $gte: moment() }, isWin: null, active: true })
      .sort('-ctime')
      .exec()
      .then(rx => {
        req.io.emit('predictNow', rx);
        return res.status(200).json({message: 'EDIT SUCCESS'});
      });

    })
}

function SORT_TEAM_TOURNAMENT(req, res, next) {
  Tournament
    .findOne({ _id: req.body.id })
    .exec()
    .then(function(result) {
      if (!result) {
        return res.status(404).json('NOT FOUND');
      } else {
        // var random = _.shuffle(result.players).chunk(2);
        // var random = _.chunk(_.shuffle(result.players), 2);
        var random = _.shuffle(result.players);
        result.players = _.chunk(random, 2);
        result.save(function(err, resp) {
          if (err) return res.status(400).json(err);
          return res.status(200).json(result);
        })
      }
    })
}


function DELETE (req, res, next) {
  Predict
    .findOne({_id: req.params.id})
    .exec()
    .then(function(tour){
      if(!tour.isWin) {
        console.log('ok >>>')
        if(tour.players.teamA.length) {
          console.log('have teamA');
          _.forEach(tour.players.teamA, team => {
            PAY_CREDIT.ADD(team.userId, tour.credit, `Hoàn trả xu vì trận đấu không diễn ra: ${tour.credit}`);
          });
        }
        if(tour.players.teamB.length) {
          console.log('have teamB');
          _.forEach(tour.players.teamB, team => {
            PAY_CREDIT.ADD(team.userId, tour.credit, `Hoàn trả xu vì trận đấu không diễn ra: ${tour.credit}`);
          })
        }
      }
      tour.remove(function(err) {
        if(err) {
          return res.status(401).json('NOT DELETE');
        } else {
          return res.status(200).json({message: 'DELETED SUCCESS'})
        }
      })
    })
}

function SET_WIN (req, res, next) {
  Predict.findOne({_id: req.params.id, active: true})
  .exec()
  .then(r => {
    if(!r) return res.status(404).json({message: 'Trận đấu kông tồn tại'});

    var currentTeam;
    var currentTeamName;
    var slTeamA = r.players.teamA.length;
    var slTeamB = r.players.teamB.length;
    if(req.body.isWin === 'teamA') {
      currentTeamName = r.teams.teamA;
      currentTeam = r.players.teamA;
    } else {
      currentTeamName = r.teams.teamB;
      currentTeam = r.players.teamB;
    }

    const payForTeamWin = (teamWin, award, refund) => {
      if(refund) {
        r.players[teamWin].forEach(team => {
          PAY_CREDIT.ADD(team.userId, award, `Hoàn trả lại xu vì không đủ người tham gia 1 trong 2 đội: ${award} | Đội đã dự đoán: ${r.teams[teamWin]}  | Trận đấu dự đoán: ${r.name} | ${r._id}`);
        });
      } else {
        r.players[teamWin].forEach(team => {
          PAY_CREDIT.ADD(team.userId, award, `Phần thưởng cho đội dự đoán thắng: ${award} | Đội đã dự đoán: ${r.teams[teamWin]}  | Trận đấu dự đoán: ${r.name} | ${r._id}`);
        });
      }

      Predict.find({ isWin: {$ne:null} })
      .sort('-ctime')
      .exec()
      .then(rx => {
        req.io.emit('predictHistory', rx);
      });

      Predict.find({ startRun: { $lte: moment() }, endRun: { $gte: moment() }, isWin: null, active: true })
      .sort('-ctime')
      .exec()
      .then(rx => {
        req.io.emit('predictNow', rx);
        res.status(200).json({message: 'Công bố kết quả thành công'});
      });
    }

    Predict.findByIdAndUpdate(
        req.params.id,
        { isWin: currentTeamName, endRun: moment() },
        function(err, tour) {
          if (err) {
            return res.status(500).json({message: err});
          } else {
            if(slTeamA < 1 || slTeamB < 1 ) {
              if(slTeamA > 1) {
                var paidCredit = parseInt(r.credit)
                payForTeamWin('teamA', paidCredit, true);
              } else {
                var paidCredit = parseInt(r.credit)
                payForTeamWin('teamB', paidCredit, true);
              }
            } else {
              if(req.body.isWin === 'teamA') {
                var paidCredit = parseInt(r.credit) + Math.floor((slTeamB * parseInt(r.credit)) / slTeamA);
                payForTeamWin('teamA', paidCredit, false);
              }

              if(req.body.isWin === 'teamB') {
                var paidCredit = parseInt(r.credit) + Math.floor((slTeamA * parseInt(r.credit)) / slTeamB);
                payForTeamWin('teamB', paidCredit, false);
              }
            }

          }
        }
    );
  })
  .catch(err => {
    console.log(err);
  })
}

function EXPORT_TEAM_TOURNAMENT(req, res, next) {
    if(!req.params.id) { return res.status(404).json({message: 'NOT FOUND'})}
    var index = 1;
    Tournament
      .findOne({_id: req.params.id})
      .populate({
        path: 'players',
        select: '_id name ownerId isWin phone ctime',
        populate: {
          path: 'ownerId',
          model: 'User',
          select: 'name',
        }
      })
      .exec()
      .then(function(t) {
        if (!t) {
          return res.status(404).json('NOT FOUND');
        } else {
          var slugName = slug(t.name, { lower: true });
          var conf ={};

          conf.rows = [];
          _.each(t.players, function(p){
            conf.rows.push([index, p.name, p.phone, p.ownerId.name, moment(p.ctime).format('DD-MM-YYYY')]);
            index++;
          })

            conf.name = "Team";
            conf.cols = [{
                caption:'STT',
                type:'number',
                width: 5
            },{
                caption:'Tên đội',
                type:'string',
                width: 20
            },{
                caption:'SDT',
                type:'string',
                width: 20
            },{
                caption:'Chủ phòng máy',
                type:'string',
                width: 40
            },{
                caption:'Ngày tham gia',
                 type:'string',
                 width: 20
            }];

            res.set('Content-Type', 'application/vnd.openxmlformats');
            res.set('Content-Disposition', 'attachment; filename=' + slugName + '.xlsx') ;
            var base64Arr = new Buffer(nodeExcel.execute(conf), 'binary'); // cols & rows are utf-8
            res.send(base64Arr);
          // })
        }
      });
}
