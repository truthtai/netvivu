var router = require('express').Router();
var User = require('../models/user');
var Type = require('../models/type');
var Log = require('../models/log');
var Promise = require('bluebird');
var moment = require('moment');
var crypto = require('crypto');
var nodemailer = require('nodemailer');

var AUTH_TOKEN = require('../middleware/authAccessToken');
var PERMISSION = require('../middleware/permission');
var CREDIT = require('../middleware/credit');
var LOG = require('../middleware/log');
var SMS = require('../middleware/sms');
var PAYCREDIT = require('../middleware/payCredit');

var _ = require('lodash');
var jwt = require('jsonwebtoken');
var CONFIG = require('../config');
var ERR_CODE_CARD = require('../errorCardCode');
var xu = 200;
router.get('/', [AUTH_TOKEN, PERMISSION('GET_ALL_USER'), GET_ALL_USER]);
router.get('/reset-password', REQUEST_RESET_PASSWORD);
router.post('/reset-password', RESET_PASSWORD);
router.put('/', AUTH_TOKEN, PERMISSION('EDIT_CURRENT_USER'), EDIT_CURRENT_USER);
router.delete('/:id', [AUTH_TOKEN, PERMISSION('DELETE_USER'), DELETE_USER]);
// router.post('/upgrade', []);
router.put('/:id', AUTH_TOKEN, PERMISSION('EDIT_USER'), EDIT_USER);

router.post('/', ADD_USER, extractInfo);
router.post('/login', LOGIN_USER, extractInfo);

router.get('/me', AUTH_TOKEN, PERMISSION('GET_CURRENT_USER'), GET_CURRENT_USER);
router.get('/me/token', AUTH_TOKEN, GET_CURRENT_USER_TOKEN, extractInfo);

router.put('/me/extract-credit/:id', AUTH_TOKEN, PERMISSION('REFUND_CREDIT_USER'), REFUND_CREDIT_USER);

router.post('/me/upgradeCPM', AUTH_TOKEN, PERMISSION('UPGRADE_CPM'), UPGRADE_CPM, extractInfo);
// router.post('/me/upgradeCTV', AUTH_TOKEN, PERMISSION('UPGRADE_CTV'), UPGRADE_CTV, extractInfo);

router.get('/me/extendCPM', AUTH_TOKEN, PERMISSION('EXTEND_CPM'), EXTEND_CPM, extractInfo);

router.get('/me/user', AUTH_TOKEN, PERMISSION('GET_USER_BY_OWNER'), GET_USER_BY_OWNER);

// router.post('/me/user/:id/upgradeCPM', AUTH_TOKEN, PERMISSION('UPGRADE_CPM_BY_CTV'), UPGRADE_CPM_BY_CTV);
// router.get('/me/user/:id/extendCPM', AUTH_TOKEN, PERMISSION('EXTEND_CPM_BY_CTV'), EXTEND_CPM_BY_CTV, extractInfo);

router.get('/me/searchUser', AUTH_TOKEN, PERMISSION('GET_MEMBER_BY_OWNER'), GET_MEMBER_BY_OWNER);
router.post('/me/addUser', AUTH_TOKEN, PERMISSION('ADD_USER_BY_OWNER'), ADD_USER_BY_OWNER);
router.delete('/me/user/:id', AUTH_TOKEN, PERMISSION('DELETE_USER_BY_OWNER'), DELETE_USER_BY_OWNER);

router.get('/me/credit', AUTH_TOKEN, PERMISSION('GET_CREDIT_USER'), GET_CREDIT_USER);
router.post('/me/credit/add', AUTH_TOKEN, PERMISSION('ADD_CREDIT_USER'), ADD_CREDIT_USER, extractInfo);

router.get('/me/logs', AUTH_TOKEN, PERMISSION('GET_LOGS_CURRENT_USER'), GET_LOGS_CURRENT_USER);
router.get('/online', AUTH_TOKEN, PERMISSION('CHECK_USER_ONLINE'), CHECK_USER_ONLINE);
router.post('/notif', AUTH_TOKEN, PERMISSION('SEND_NOTIF_TO_USER_ONLINE'), SEND_NOTIF_TO_USER_ONLINE);

router.get('/report', AUTH_TOKEN, PERMISSION('REPORT_USER'), REPORT_USER);
router.get('/members', AUTH_TOKEN, SEARCH_USER);
module.exports = router;

function REQUEST_RESET_PASSWORD(req, res) {
  User.findOne({
      email: req.query.email
    })
    .exec()
    .then(function (user) {
      if (user) {
        crypto.randomBytes(5, function (ex, buf) {
          user.resetPasswordCode = buf.toString('hex');
          // var emailTemplate = _.template('Please click this link to reset your password <a href="<%= activationLink %>"><%= activationLink %></a>');
          var transporter = nodemailer.createTransport({
            service: 'Gmail',
            auth: {
              user: CONFIG.EMAIL.USER,
              pass: CONFIG.EMAIL.PASS
            }
          });
          var mailOptions = {
            from: 'support@vivuplay.vn',
            to: user.email,
            subject: 'Yêu cầu cấp lại mật khẩu tại VIVUPLAY.VN',
            text: `Bạn vừa yêu cầu cấp lại mật khẩu. Mã cấp lại mật khẩu của bạn là: ${user.resetPasswordCode}`,
            html: `Bạn vừa yêu cầu cấp lại mật khẩu. Mã cấp lại mật khẩu của bạn là: <strong>${user.resetPasswordCode}</strong>`
          };

          transporter.sendMail(mailOptions, function (error, info) {
            if (error) {
              console.log(error);
              res.status(400).json({
                message: 'Có lỗi khi gửi Mail'
              });
            } else {
              console.log('Message sent: ' + info.response);
              user.save();
              return res.status(201).json({ message: 'Yêu cầu thành công. Vui lòng kiểm tra Email'});
            };
          });
        });
      } else {
        return res.status(400).json({message: 'Thông tin không chính xác'});
      }
    });
}

function RESET_PASSWORD(req, res) {
  var hash = crypto
    .createHmac("md5", CONFIG.SECRET)
    .update(req.body.password)
    .digest('hex');

  User.findOne({
    email: req.query.email,
    resetPasswordCode: req.body.code
  })
  .exec()
  .then(function(user) {
    if (user) {
      user.resetPasswordCode = '';
      user.password = hash;
      user.save();
      return res.status(200).json({ message: 'Đổi mật khảu thành công'});
    } else {
      return res.status(400).json({message: 'Thông tin không chính xác'});
    }
  });
}

function SEARCH_USER(req, res) {
  User
    .find({ $or: [ { 'username': {'$regex' : `${req.query.search}`, '$options' : 'i' } }, { 'phone': {'$regex' : `${req.query.search}`, '$options' : 'i' } } ]})
    .exec()
    .then(function(users) {
      var userData = _.map(users, function(user) {
        return _.pick(user.toJSON(), ['_id', 'username', 'name', 'address', 'province', 'city', 'phone', 'email']);
      });
      // console.log(userData);
      return res.status(200).json(userData);
    })
    .catch(function(err) {
      return res.status(400).json(err)
    })
}

function CHECK_USER_ONLINE(req, res) {
  return res.status(200).json({count: req.io.sockets.adapter.rooms['connected'].length || 0});
}

function SEND_NOTIF_TO_USER_ONLINE(req, res) {
  if(!req.body.message) {
    return res.status(400).json('ERR');
  }
  req.io.to('connected').emit('notif', req.body.message);
  return res.status(200).json({message: 'SUCCESS'});
}

function REPORT_USER(req, res, next) {
  var group ={};
  group.$group = { _id: '$type', 'count':{ $sum:1 }};
  User.aggregate(group)
  .exec(function (err, users) {
    if(err) {
      return res.status(500).json(err);
    } else {
      User.populate( users, {path: "_id", model: 'Type', select: 'name', options: { sort: '-name'} }, function(err,results) {
        if (err) return res.status(500).json(err);
        return res.status(200).json(results);
      });
    }
  });
}

function GET_ALL_USER(req, res, next) {
  var query = buildFilterQuery(req);
  User
    .find(query)
    .sort('type')
    .populate({
      path: 'type',
      // populate: {
      //   path: 'permission',
      //   model: 'Permission'
      // }
    })
    .exec()
    .then(function(users) {
      var userData = _.map(users, function(user) {
        return _.omit(user.toJSON(), ['password']);
      });
      // console.log(userData);
      return res.status(200).json(userData);
    })
    .catch(function(err) {
      return res.status(400).json(err)
    })
}

function ADD_USER(req, res, next) {
  if (!req.body.username || !req.body.password || !req.body.email || !req.body.phone) {
    return res.status(400).json({
      message: 'USERNAME & PASSWORD & EMAIL & PHONE ARE REQUIRED'
    });
  }

  var hash = crypto
    .createHmac("md5", CONFIG.SECRET)
    .update(req.body.password)
    .digest('hex');


  var user = {
    email: req.body.email,
    name: req.body.name,
    username: req.body.username,
    gender: req.body.gender || '',
    phone: req.body.phone || '',
    address: req.body.address || '',
    province: req.body.province || '',
    city: req.body.city || '',
    utime: req.body.utime,
    logIn: false,
    status: 'active',
    credit: 0,
    utime: new Date(),
    ctime: new Date()
  }


  Type
    .findOne({
      name: 'member'
    })
    .exec()
    .then(function(type) {
      if (type) {
        user.type = type;
        user.password = hash;
        User.create(user, function(err, resp) {
          if (err) return res.status(400).json({
            message: 'SDT hoặc Username hoặc Email đã được sử dụng'
          });
          SMS.SEND(user.phone, 'REGISTER_ACCOUNT');
          req.user = resp;
          next();

          // return res.status(200).json({ message: 'REGISTER SUCCESS' });
        });
      } else {
        if (err) return res.status(404).json({
          message: 'NOT FOUND TYPE ACCOUNT'
        });
      }
    })
}

function ADD_USER_BY_OWNER(req, res, next) {
  if (!req.body.username || !req.body.password || !req.body.email || !req.body.phone) {
    return res.status(400).json({
      message: 'USERNAME & PASSWORD & EMAIL & PHONE ARE REQUIRED'
    });
  }

  var hash = crypto
    .createHmac("md5", CONFIG.SECRET)
    .update(req.body.password)
    .digest('hex');


  var user = {
    email: req.body.email,
    name: req.body.name,
    username: req.body.username,
    gender: req.body.gender || '',
    phone: req.body.phone || '',
    address: req.body.address || '',
    province: req.body.province || '',
    city: req.body.city || '',
    utime: req.body.utime,
    logIn: false,
    status: 'active',
    credit: 0
  }

  Type
    .findOne({
      name: 'HV'
    })
    .exec()
    .then(function(type) {
      if (type) {
        user.type = type;
        user.password = hash;
        user.ownerId = req.user._id;
        User.create(user, function(err, resp) {
          if (err) {
            console.log(err);
            return res.status(400).json({
              message: 'ERROR BAD REQUEST'
            })
          }
          // SMS.SEND(req.user.phone, 'REGISTER_ACCOUNT');
          // SMS.SEND(resp.phone, 'REGISTER_ACCOUNT');
          LOG.ADD('ADD_USER', req.user._id, 'Thêm tài khoản: ' + resp.email + ' | ' + resp.phone + ' | ' + resp.type.name);
          return res.status(200).json({
            message: 'REGISTER SUCCESS'
          });
        });
      } else {
        if (err) return res.status(404).json({
          message: 'NOT FOUND TYPE ACCOUNT'
        });
      }
    })
}

function LOGIN_USER(req, res, next) {

  if (!req.body.password || !req.body.username) {
    return res.status(400).json({
      message: 'USER AND PASSWORD ARE REQUIRED'
    });
  }
  var ip = req.headers["X-Forwarded-For"] || req.connection.remoteAddress;
  if (ip.length < 15) {
    ip = ip;
  } else {
    var nyIP = ip.slice(7);
    ip = nyIP;
  }

  var hash = crypto
    .createHmac("md5", CONFIG.SECRET)
    .update(req.body.password)
    .digest('hex');

  User.findOne({
    username: req.body.username
  }, function(err, user) {

    if (err) throw err;

    if (!user) {
      return res.status(404).json({
        message: 'User not found.'
      });
    } else if (user) {

      // check if password matches
      if (user.password != hash) {
        return res.status(401).json({
          message: 'Wrong password.'
        });
      } else {
        // SMS.SEND(user.phone, 'LOGIN_ACCOUNT');
        if(!user.loggedIP) {
          user.loggedIP = [];
          user.loggedIP.push({ ip: ip, ctime: moment()})
        } else {
          user.loggedIP.push({ ip: ip, ctime: moment()})
        }
        user.save();
        req.user = user;
        next();
      }

    }
  })

}

function extractInfo(req, res, next) {
  var userJSON = req.user.toJSON();
  Type
    .findOne({
      _id: userJSON.type
    })
    .exec()
    .then(function(type) {
      userJSON.type = type.name;
      var userDataToken = _.omit(userJSON, ['password', 'loggedIP']);
      // create a token
      var token = jwt.sign(userDataToken, CONFIG.SECRET);
      req.io.in('user/' + userJSON._id).emit('userData', token);
      // return the information including token as JSON
      return res.status(200).json({
        accessToken: token
      });
    })
    // if user is found and password is right
}

function DELETE_USER(req, res, next) {
  User
    .findOne({
      _id: req.params.id
    })
    .exec()
    .then(function(user) {
      if (user.status === 'active') {
        user.status = 'waittingRemove';
        user.willRemoveAt = moment(new Date()).add(30, 'minutes');
        user.save();
        return res.status(200).json({
          message: 'DELETE SUCCESS'
        });
      } else {
        return res.status(401).json({
          message: 'CANNOT DELETE THIS ACCOUNT'
        });
      }
    })
    .catch(function(err) {
      return res.status(400).json(err)
    })
}

function DELETE_USER_BY_OWNER(req, res, next) {
  User
    .findOne({
      _id: req.params.id,
      ownerId: req.user._id
    })
    .exec()
    .then(function(user) {
      if (user.status === 'active') {
        user.status = 'waittingRemove';
        user.willRemoveAt = moment(new Date()).add(24, 'hours');
        user.save();
        return res.status(200).json({
          message: 'DELETE SUCCESS'
        });
      } else {
        return res.status(401).json({
          message: 'CANNOT DELETE THIS ACCOUNT'
        });
      }
    })
    .catch(function(err) {
      return res.status(400).json({ message: err })
    })
}

function UPGRADE_CPM(req, res, next) {
  var _expType;
  User.findOne({
      _id: req.user._id
    })
    .exec().then(function(u) {
      req.user = u;
      if (req.user.credit < xu) {
        return res.status(401).json({
          message: 'Xu không đủ'
        })
      }
      if (!req.body.address || !req.body.district || !req.body.city || !req.body.gpkd || !req.body.bank) {
        return res.status(401).json({
          message: 'Vui lòng nhập đủ thông tin trước khi nâng cấp tài khoản'
        })
      }
      if (req.user.expType) {
        _expType = moment(req.user.expType).add(90, 'days');
      } else {
        _expType = moment(new Date()).add(90, 'days');
      }

      var payCredit = req.user.credit - xu;
      var info = {
        address: req.body.address,
        district: req.body.district,
        city: req.body.city,
        gpkd: req.body.gpkd,
        bank: req.body.bank,
        businessName: req.body.businessName,
      };
      Type
        .findOne({
          name: 'CPM'
        }).exec().then(function(type) {
          User.update({
            _id: req.user._id
          }, {
            credit: payCredit,
            extraInfo: info,
            expType: _expType,
            type: type._id
          }, function(err, resp) {
            if (err) {
              return res.status(401).json(resp)
            }
            User.findOne({
              _id: req.user._id
            }).exec().then(function(user) {
              req.user = user;
              SMS.SEND(user.phone, 'UPGRADE_CPM');
              LOG.ADD('UPGRADE_CPM', req.user._id, 'Nâng cấp CPM thành công | ' + resp.email + ' | ' + resp.phone + ' | ' + user.type);
              next();
            })
          });
        })
    })


}

function EXTEND_CPM_BY_CTV(req, res, next) {
  var _expType;
  User.findOne({_id: req.params.id})
  .exec()
  .then(function(ut){
    if(!ut) { return res.status(404).json({message: 'NOT FOUND'})};
    if (req.user.type === 'CTV') {
      User.findOne({
          _id: req.user._id
        })
        .exec().then(function(u) {
          req.user = u;
          if (req.user.credit < xu) {
            return res.status(401).json({
              message: 'Xu không đủ'
            })
          }
          if (!req.user.extraInfo) {
            return res.status(401).json({
              message: 'Vui lòng nhập đủ thông tin trước khi gia hạn tài khoản'
            })
          }
          if (ut.expType || ut.expType >= new Date()) {
            _expType = moment(ut.expType).add(90, 'days');
          } else {
            _expType = moment(new Date()).add(90, 'days');
          }

          var payCredit = req.user.credit - xu;
          User.update({
            _id: req.user._id
          }, {
            credit: payCredit
          }, function(err, resp) {
            if (err) {
              return res.status(401).json(resp)
            }
            User.findOne({
              _id: req.user._id
            }).exec().then(function(user) {
              User.update({
                _id: ut._id
              }, {
                expType: _expType
              }, function(err, resp) {
                if (err) {
                  return res.status(401).json(resp)
                }

              SMS.SEND(user.phone, 'EXTEND_CPM');
              SMS.SEND(ut.phone, 'EXTEND_CPM');
              LOG.ADD('EXTEND_CPM', ut._id, 'Gia hạn CPM thành công | Bởi: ' + req.name + ' ( ' + req.email + ' ) ');
              LOG.ADD('EXTEND_CPM_BY_CTV', req.user._id, 'Gia hạn CPM thành công: ' + req.user.email + ' | ' + req.user.phone + ' | Xu hiện tại: ' + req.user.credit);
              req.user = user;
              next();
            })
        })
      })
    })
    } else {
      return res.status(401).json({
        message: 'Tài khoản không thể thực thi!'
      })
    }
  })
}

function EXTEND_CPM(req, res, next) {
  var _expType;
  if (req.user.type === 'CPM') {
    User.findOne({
        _id: req.user._id
      })
      .exec().then(function(u) {
        req.user = u;
        if (req.user.credit < xu) {
          return res.status(401).json({
            message: 'Xu không đủ'
          })
        }
        if (!req.user.extraInfo) {
          return res.status(401).json({
            message: 'Vui lòng nhập đủ thông tin trước khi gia hạn tài khoản'
          })
        }
        if (req.user.expType) {
          _expType = moment(req.user.expType).add(90, 'days');
        } else {
          _expType = moment(new Date()).add(90, 'days');
        }

        var payCredit = req.user.credit - xu;
        Type
          .findOne({
            name: 'CPM'
          }).exec().then(function(type) {
            User.update({
              _id: req.user._id
            }, {
              credit: payCredit,
              expType: _expType,
              type: type._id
            }, function(err, resp) {
              if (err) {
                return res.status(401).json(resp)
              }
              User.findOne({
                _id: req.user._id
              }).exec().then(function(user) {
                SMS.SEND(user.phone, 'EXTEND_CPM');
                LOG.ADD('EXTEND_CPM', req.user._id, 'Gia hạn CPM thành công: ' + user.email + ' | ' + user.phone + ' | Xu hiện tại: ' + user.credit);
                req.user = user;
                next();
              })
            });
          })
      })
  } else {
    return res.status(401).json({
      message: 'Tài khoản không thể thực thi!'
    })
  }

}

function EDIT_CURRENT_USER(req, res, next) {

}

function GET_CURRENT_USER(req, res, next) {
  User
    .findOne({
      _id: req.user._id
    })
    .populate({
      path: 'type',
      populate: {
        path: 'permission',
        model: 'Permission'
      }
    })
    .exec()
    .then(function(users) {
      var userData = _.omit(users.toJSON(), ['password']);
      // console.log(userData);
      return res.status(200).json(userData);
    })
    .catch(function(err) {
      return res.status(400).json(err)
    })


}

function GET_CURRENT_USER_TOKEN(req, res, next) {
  User
    .findOne({
      _id: req.user._id
    })
    .exec()
    .then(function(user) {
      if(!user) { return res.status(404).json({message: 'NOT FOUND'}) }
      var userData = _.omit(user, ['password']);
      // console.log(userData);
      req.user = userData;
      next();
    })
    .catch(function(err) {
      return res.status(400).json(err)
    })
}

function GET_CREDIT_USER(req, res, next) {
  User
    .findOne({
      _id: req.user._id
    })
    .exec()
    .then(function(users) {
      var userData = _.pick(users.toJSON(), ['credit']);
      return res.status(200).json(userData);
    })
    .catch(function(err) {
      return res.status(400).json(err)
    })
}


function EDIT_USER(req, res, next) {
  var id = req.params.id;

  var info = _.omit(req.body, ['password', 'credit', '_id']);
  Type.findOne({_id: req.body.type})
  .exec().then(function(t){
    info.utime = new Date();

    if (req.user.type === 'admin' && t.name === 'CPM') {
      info.expType = moment(new Date()).add(90, 'days');
    }

    User.update({
      _id: id
    }, info, function(err, resp) {
      if (err) {
        return res.status(401).json(err)
      }
      return res.status(200).json(resp)
    });
  })

}

function ADD_CREDIT_USER(req, res, next) {
  if (!req.body.issuer || !req.body.cardSerial || !req.body.cardCode) {
    return res.status(401).json({
      message: 'BAD REQUEST'
    })
  }

  var data = {
    issuer: req.body.issuer,
    cardSerial: req.body.cardSerial,
    cardCode: req.body.cardCode,
    amount: '500000'
  };

  crypto.randomBytes(15, function(err, buf) {
    if (err) throw err;
    data.transRef = buf.toString('hex');
    data.partnerCode = CONFIG.GATEWAY_CARD.PARTNER_CODE;
    data.password = CONFIG.GATEWAY_CARD.PASSWORD;
    data.accountId = req.user.phone;
    data.signature = crypto.createHash('md5').update(data.issuer + data.cardCode + data.transRef + data.partnerCode + data.password + CONFIG.GATEWAY_CARD.SERECT_KEY).digest('hex');
    CREDIT.USE_CARD(data)
      .then(function(resp) {
        if (resp.status === '01') {
          User.update({
            _id: req.user._id
          }, {
            credit: (parseInt(resp.amount)/1000) + req.user.credit
          }, function(er, r) {
            if (er) {
              return res.status(401).json(er);
            }
            SMS.SEND(req.user.phone, 'ADD_CREDIT_ACCOUNT');
            LOG.ADD('ADD_CREDIT', req.user._id, 'Nạp xu thành công: ' + resp.amount + ' | ' + resp.cardSerial + ' | ' + resp.cardCode).then(function(l) {
              User.findOne({
                _id: req.user._id
              }).exec().then(function(m){
                req.user = m;
                next();
              });
            })
          });
        } else {
          LOG.ADD('ERR_ADD_CREDIT', req.user._id, 'Nạp xu thất bại: ' + resp.description + ' | ' + resp.cardSerial + ' | ' + resp.cardCode);
          return res.status(401).json({
            message: ERR_CODE_CARD.ERR_CODE[resp.status]
          });
        }
      })
      .catch(function(err) {
        LOG.ADD('ERR_ADD_CREDIT', req.user._id, 'Nạp xu thất bại: ' + JSON.stringify(err));
        return res.status(401).json(err);
      })
  });


}


function GET_USER_BY_OWNER(req, res, next) {
  User
    .find({
      ownerId: req.user._id
    })
    .populate({
      path: 'type',
      populate: {
        path: 'permission',
        model: 'Permission'
      }
    })
    .sort({ctime: -1})
    .exec()
    .then(function(users) {
      var userData = _.map(users, function(user) {
        return _.omit(user.toJSON(), ['password']);
      });
      // console.log(userData);
      return res.status(200).json(userData);
    })
    .catch(function(err) {
      return res.status(400).json(err)
    })
}

function GET_LOGS_CURRENT_USER(req, res, next) {
  Log
    .find({
      userId: req.user._id
    })
    .sort({ctime: -1})
    .exec()
    .then(function(logs) {
      if (!logs) {
        return res.status(404).json('NOT FOUND');
      } else {
        return res.status(200).json(logs)
      }
    })
}

function UPGRADE_CTV(req, res, next) {
  User.findOne({
      _id: req.user._id
    })
    .exec().then(function(u) {
      req.user = u;
      if (req.user.credit < 50) {
        return res.status(401).json({
          message: 'Xu không đủ'
        })
      }

      var payCredit = req.user.credit - 50;
      Type
        .findOne({
          name: 'CTV'
        }).exec().then(function(type) {
          User.update({
            _id: req.user._id
          }, {
            credit: payCredit,
            type: type._id
          }, function(err, resp) {
            if (err) {
              return res.status(401).json(resp)
            }
            User.findOne({
              _id: req.user._id
            }).exec().then(function(user) {
              req.user = user;
              SMS.SEND(user.phone, 'UPGRADE_CTV');
              LOG.ADD('UPGRADE_CTV', req.user._id, 'Nâng cấp CTV thành công');
              next();
            })
          });
        })
    })
}

function GET_MEMBER_BY_OWNER(req, res, next) {
  var query = buildFilterQuery(req);
  if(!req.query.search) {
    return res.status(200).json([]);
  }

  Promise.coroutine(function* () {
    const type = yield Type.find({id: '1'}).exec();
    const users = yield User.find(query).populate({ path: 'type' }).exec();
    const userData = yield Promise.map(users, function(user)  { return _.omit(user.toJSON(), ['password', 'credit']) });
    const xm = yield Promise.filter(userData, function(xx)    { return  xx.type.name !== 'admin' &&
                                                                        xx.type.name !== 'CTV' &&
                                                                        xx.type.name !== 'BTV' })
    return xm;
  })().then(function(user) { return res.status(200).json(user) })
      .catch(function(err) { return res.status(401).json(err) })
}

function UPGRADE_CPM_BY_CTV(req, res, next) {
  var _expType;

  Promise.coroutine(function* () {
    const userTarget = yield User.findOne({ _id: req.params.id }).exec();
    if(!userTarget) return res.status(404).json({message: 'NOT FOUND'});

    const userCurrent = yield User.findOne({ _id: req.user._id }).exec();
    if (req.user.credit < 400) return res.status(401).json({ message: 'Xu không đủ' });

    if (!req.body.address ||
        !req.body.district ||
        !req.body.city ||
        !req.body.gpkd ||
        !req.body.bank) return res.status(401).json({ message: 'Vui lòng nhập đủ thông tin trước khi nâng cấp tài khoản' });

    req.user = userCurrent;
    const payCredit = req.user.credit - xu;
    const info = {
      address: req.body.address,
      district: req.body.district,
      city: req.body.city,
      gpkd: req.body.gpkd,
      bank: req.body.bank,
      businessName: req.body.businessName,
    };

    const userUpdate = yield User.findByIdAndUpdate(req.user._id,{ credit: payCredit }).exec();
    const sendLOG = yield LOG.ADD('PAY_CREDIT_UPGRADE_CPM', req.user._id, 'Thanh toán Xu để nâng cấp CPM thành công');
    const type = yield Type.findOne({ name: 'CPM' }).exec();
    const userTargetUpdate = yield User.findByIdAndUpdate(userTarget._id, {
                                                          extraInfo: info,
                                                          expType: moment(new Date()).add(90, 'days'),
                                                          type: type._id,
                                                          ownerId: req.user._id
                                                          }).exec();
    const getAgainUser = yield User.findOne({ _id: req.user._id }).exec();
    SMS.SEND(getAgainUser.phone, 'UPGRADE_CPM');
    SMS.SEND(userTarget.phone, 'UPGRADE_CPM');
    LOG.ADD('UPGRADE_CPM', userTarget._id, 'Nâng cấp CPM thành công');
    LOG.ADD('UPGRADE_CPM', getAgainUser._id, 'Nâng cấp CPM thành công | ' + userTarget.username + ' | ' + userTarget.email + ' | ' + userTarget.phone);

    return res.status(200).json({message: 'UPGRADE CPM IS SUCCESS'});
  })().then().catch(function(err) { return res.status(401).json(err) })

  // User.findOne({
  //     _id: req.params.id
  //   }).exec()
  //   .then(function (ut) {
  //     if(!ut) { return res.status(404).json({message: 'NOT FOUND'})};
  //     User.findOne({
  //         _id: req.user._id
  //       })
  //       .exec().then(function(u) {
  //         req.user = u;
  //         if (req.user.credit < 400) {
  //           return res.status(401).json({
  //             message: 'Xu không đủ'
  //           })
  //         }
  //         if (!req.body.address || !req.body.district || !req.body.city || !req.body.gpkd || !req.body.bank) {
  //           return res.status(401).json({
  //             message: 'Vui lòng nhập đủ thông tin trước khi nâng cấp tài khoản'
  //           })
  //         }
  //         // if (req.user.expType) {
  //         //   _expType = moment(req.user.expType).add(90, 'days');
  //         // } else {
  //         //   _expType = moment(new Date()).add(90, 'days');
  //         // }
  //
  //         var payCredit = req.user.credit - 300;
  //         var info = {
  //           address: req.body.address,
  //           district: req.body.district,
  //           city: req.body.city,
  //           gpkd: req.body.gpkd,
  //           bank: req.body.bank,
  //           businessName: req.body.businessName,
  //         };
  //
  //         User.update({
  //           _id: req.user._id
  //         }, {
  //           credit: payCredit,
  //         }, function(err, resp) {
  //           if (err) {
  //             return res.status(401).json(err)
  //           }
  //           LOG.ADD('PAY_CREDIT_UPGRADE_CPM', req.user._id, 'Thanh toán Xu để nâng cấp CPM thành công');
  //
  //           Type
  //             .findOne({
  //               name: 'CPM'
  //             }).exec().then(function(type) {
  //               User.update({
  //                 _id: ut._id
  //               }, {
  //                 extraInfo: info,
  //                 expType: moment(new Date()).add(90, 'days'),
  //                 type: type._id,
  //                 ownerId: req.user._id
  //               }, function(err, resp) {
  //                 if (err) {
  //                   return res.status(401).json(resp)
  //                 }
  //                 User.findOne({
  //                   _id: req.user._id
  //                 }).exec().then(function(user) {
  //                   req.user = user;
  //                   SMS.SEND(user.phone, 'UPGRADE_CPM');
  //                   SMS.SEND(ut.phone, 'UPGRADE_CPM');
  //                   LOG.ADD('UPGRADE_CPM', ut._id, 'Nâng cấp CPM thành công');
  //                   LOG.ADD('UPGRADE_CPM', req.user._id, 'Nâng cấp CPM thành công | ' + ut.username + ' | ' + ut.email + ' | ' + ut.phone);
  //                   return res.status(200).json({message: 'UPGRADE CPM IS SUCCESS'})
  //                 })
  //               });
  //             })
  //
  //         })
  //
  //
  //       })
  //   })
  //

}

function REFUND_CREDIT_USER(req, res, next) {
  var msg;

  if(req.body.credit === '' || req.body.mcredit === '' || !req.body.action) {
    return res.status(500).json({message: 'Vui lòng điền đủ thông tin'});
  }
  User.findOne({
      username: req.params.id
    })
    .exec().then(function(u) {
      if(!u) {
        return res.status(404).json({message: 'NOT FOUND'});
      }
      req.body.credit = parseInt(req.body.credit);
      req.body.mcredit = parseInt(req.body.mcredit);
      u.credit = parseInt(u.credit);

      if(req.body.action === 'add') {
        u.credit = u.credit + req.body.credit;
        msg = 'Hoàn trả xu thành công';
      } else {
        msg = 'Thu hồi xu thành công';
        if (u.credit < req.body.mcredit) {
          u.credit = 0;
        } else {
          u.credit = u.credit - req.body.mcredit;
        }
      }
      u.save(function (err, r) {
        if(err) {
          return res.status(401).json({message: 'Lỗi'});
        }
        LOG.ADD('REFUND_CREDIT_USER', u._id, msg + ' : ' + req.body.credit + ' | Lý do: ' + req.body.reason);
        return res.status(200).json({message: 'REFUND SUCCESS'});
      })
    })

}

function buildFilterQuery(req) {
  var query = {};
  query.$and = [];

  if (req.query.search) {
    query.$text = {
      '$search': req.query.search
    };
  }

  if (query.$and.length === 0) {
    delete query.$and;
  }

  return query;
}
