var router = require('express').Router();
var Tournament = require('../models/tournament');
var Player = require('../models/player');
var User = require('../models/user');
var crypto = require('crypto');
var moment = require('moment');
var _ = require('lodash');
var fs = require('fs');
var path = require('path');
var Log = require('../models/log');
var nodeExcel = require('excel-export');
var Promise = require('bluebird');
var slug = require('slug');

var AUTH_TOKEN = require('../middleware/authAccessToken');
var PERMISSION = require('../middleware/permission');
var UPLOAD = require('../middleware/upload');
var SMS = require('../middleware/sms');
var LOG = require('../middleware/log');
var jwt = require('jsonwebtoken');
var CONFIG = require('../config');

router.get('/', GET_ALL_TOURNAMENT);
router.get('/:id', GET_TOURNAMENT);
router.delete('/:id', AUTH_TOKEN, PERMISSION('DELETE_TOURNAMENT'), DELETE_TOURNAMENT);
router.post('/', AUTH_TOKEN, PERMISSION('ADD_TOURNAMENT'), UPLOAD, ADD_TOURNAMENT);
router.put('/:id', AUTH_TOKEN, PERMISSION('EDIT_TOURNAMENT'), EDIT_TOURNAMENT);
router.post('/join', AUTH_TOKEN, PERMISSION('JOIN_TOURNAMENT'), CHECK_STATUS_TOURNAMENT, PAY_CREDIT_FOR_TOURNAMENT, JOIN_TOURNAMENT);
router.post('/sort', AUTH_TOKEN, PERMISSION('SORT_TEAM_TOURNAMENT'), SORT_TEAM_TOURNAMENT);
router.get('/export/:id', AUTH_TOKEN, PERMISSION('EXPORT_TEAM_TOURNAMENT'), EXPORT_TEAM_TOURNAMENT);


module.exports = router;
const userInfo$ = (user, io) => {
  // console.log(user);
  User.findOne({_id: user})
  .exec()
  .then((user) => {
    var userJSON = user.toJSON();
    // console.log(userJSON);
    Type
      .findOne({
        _id: userJSON.type
      })
      .exec()
      .then(function(type) {
        userJSON.type = type.name;
        // console.log(userJSON);
        var userDataToken = _.omit(userJSON, ['password', 'loggedIP']);
        // console.log(userDataToken);
        var token = jwt.sign(userDataToken, CONFIG.SECRET);
        io.to('user/' + userJSON._id).emit('userData', token);
      });
  })
}

function GET_ALL_TOURNAMENT(req, res, next) {
  Tournament
    .find()
    .populate('players', '_id name ownerId isWin phone')
    .exec()
    .then(function(t) {
      if (!t) {
        return res.status(404).json('NOT FOUND');
      } else {
        return res.status(200).json(t)
      }
    })
}

function GET_TOURNAMENT(req, res, next) {
  Tournament
    .findOne({_id: req.params.id})
    .populate({
      path: 'players',
      select: '_id name ownerId isWin phone ctime',
      populate: {
        path: 'ownerId',
        model: 'User',
        select: 'name',
      }
    })
    .exec()
    .then(function(t) {
      if (!t) {
        return res.status(404).json('NOT FOUND');
      } else {
        return res.status(200).json(t)
      }
    })
}

function ADD_TOURNAMENT(req, res, next) {
  var currentDate = new Date();
  var tournament = {
    image: req.files[0].filename,
    name: req.body.name,
    description: req.body.description,
    startReg: moment.unix(req.body.startReg).utcOffset(420).format(),
    endReg: moment.unix(req.body.endReg).format(),
    startRun: moment.unix(req.body.startRun).format(),
    endRun: moment.unix(req.body.endRun).format(),
    remindTime: moment.unix(req.body.remindTime).format(),
    randomTime: moment.unix(req.body.randomTime).format(),
    freeFirstTime: req.body.freeFirstTime,
    game: req.body.game || '',
    credit: req.body.credit,
    winner: [],
    ctime: currentDate,
    players: []
  }

  Tournament
    .create(tournament, function(err, resp) {
      if (err) return res.status(400).json(err);
      return res.status(200).json(resp);
    })
}

function CHECK_STATUS_TOURNAMENT(req, res, next) {
  User
    .findOne({
      _id: req.user._id
    })
    .populate({
      path: 'type'
    })
    .exec()
    .then(u => {
      if(u) {
        req.user = u;
        Tournament
          .findOne({
            _id: req.body.id
          })
          .exec()
          .then(function(t) {
            if (!t) {
              // return res.status(404).json({message: 'Trận không tìm thấy'});
            } else {
              // CHECK NUMBER OF TEAM}

              if (req.user.credit < t.credit) {
                return res.status(400).json({message: 'Không đủ xu thma gia'});
              }

              if (t.endReg < new Date()) {
                return res.status(400).json({message: 'Đã hết thời gian tham gia'});
              }

              if (t.startRun < new Date() && new Date() < t.endRun) {
                return res.status(400).json({message: 'Trận đấu đang diễn ra'});
              }
              req.tournament = t;
              next();
            }
          })
      }
    })

}

function PAY_CREDIT_FOR_TOURNAMENT(req, res, next) {
  Player
    .find({
      tournamentId: req.tournament._id,
      ownerId: req.user._id
    })
    .exec()
    .then(function (r) {
      if (!req.tournament.freeFirstTime || req.tournament.freeFirstTime && r.length) {
        req.user.credit = req.user.credit - req.tournament.credit;
        req.user.save(
          function (err, u) {
            if (err) {
              return res.status(500).json(err);
            } else {
              LOG.ADD('PAY_CREDIT_FOR_TOURNAMENT', req.user._id, 'Thanh toán xu tham gia trận đấu: ' + req.tournament.credit + ' | ' + req.tournament.name);
              SMS.SEND(req.user.phone, 'PAY_CREDIT_FOR_TOURNAMENT');
              next();
            }
          })
      } else {
        next();
      }
    })
}

function JOIN_TOURNAMENT(req, res, next) {
  if(!req.body.name && !req.body.phone) {
    return res.status(500).json({message: 'NAME && PHONE ARE REQUIRED'});
  }
  Player
    .create({
      name: req.body.name,
      phone: req.body.phone,
      ownerId: req.user._id,
      tournamentId: req.tournament._id
    }, function(err, player) {
      if (err) return res.status(400).json({message: err});
      Tournament
        .findOneAndUpdate({ "_id": req.tournament._id }, { $push: { "players": { $each: [player], $slice: req.tournament.numberOfTeam } } }, { safe: true, upsert: true },
          function(err, tour) {
            if (err) {
              return res.status(500).json({message: err});
            } else {
              LOG.ADD('JOIN_TOURNAMENT', req.user._id, 'Tham gia trận đấu: ' + req.tournament.name);

              SMS.SEND(req.user.phone, 'JOIN_TOURNAMENT');
              SMS.SEND(player.phone, 'JOIN_TOURNAMENT');
              userInfo$(req.user.toJSON(), req.io);
              return res.status(201).json({message: 'JOIN SUCCESS'});
            }
          }
        );
    })
}

function EDIT_TOURNAMENT(req, res, next) {
  var currentDate = new Date();
  var tournament = {
    name: req.body.name,
    description: req.body.description,
    startReg: moment.unix(req.body.startReg).utcOffset(420).format(),
    endReg: moment.unix(req.body.endReg).format(),
    startRun: moment.unix(req.body.startRun).format(),
    endRun: moment.unix(req.body.endRun).format(),
    remindTime: moment.unix(req.body.remindTime).format(),
    randomTime: moment.unix(req.body.randomTime).format(),
    freeFirstTime: req.body.freeFirstTime,
    credit: req.body.credit,
  }

  Tournament
    .update({_id:req.params.id}, tournament, function(err, resp) {
      if (err) return res.status(400).json(err);
      return res.status(200).json({message: 'EDIT SUCCESS'});
    })
}

function SORT_TEAM_TOURNAMENT(req, res, next) {
  Tournament
    .findOne({ _id: req.body.id })
    .exec()
    .then(function(result) {
      if (!result) {
        return res.status(404).json('NOT FOUND');
      } else {
        // var random = _.shuffle(result.players).chunk(2);
        // var random = _.chunk(_.shuffle(result.players), 2);
        var random = _.shuffle(result.players);
        result.players = _.chunk(random, 2);
        result.save(function(err, resp) {
          if (err) return res.status(400).json(err);
          return res.status(200).json(result);
        })
      }
    })
}


function DELETE_TOURNAMENT(req, res, next) {
  Tournament
    .findOne({_id: req.params.id})
    .exec()
    .then(function(tour){
      var filePath = path.resolve(path.join(__dirname), '../', 'uploads', tour.image);
      fs.unlinkSync(filePath);
      tour.remove(function(err) {
        if(err) {
          return res.status(401).json('NOT DELETE');
        } else {
          return res.status(200).json({message: 'DELETED SUCCESS'})
        }
      })
    })
    // .where().findOneAndRemove({_id: req.params.id}, function (err, resp) {
    //   if (err) {
    //     return res.status(401).json('NOT DELETE');
    //   } else {
    //     // t.status =
    //     fs.unlinkSync(filePath);
    //     return res.status(200).json({message: 'DELETED SUCCESS'})
    //   }
    // }) // executes
}

function EXPORT_TEAM_TOURNAMENT(req, res, next) {
    if(!req.params.id) { return res.status(404).json({message: 'NOT FOUND'})}
    var index = 1;
    Tournament
      .findOne({_id: req.params.id})
      .populate({
        path: 'players',
        select: '_id name ownerId isWin phone ctime',
        populate: {
          path: 'ownerId',
          model: 'User',
          select: 'name',
        }
      })
      .exec()
      .then(function(t) {
        if (!t) {
          return res.status(404).json('NOT FOUND');
        } else {
          var slugName = slug(t.name, { lower: true });
          var conf ={};

          conf.rows = [];
          _.each(t.players, function(p){
            conf.rows.push([index, p.name, p.phone, p.ownerId.name, moment(p.ctime).format('DD-MM-YYYY')]);
            index++;
          })

            conf.name = "Team";
            conf.cols = [{
                caption:'STT',
                type:'number',
                width: 5
            },{
                caption:'Tên đội',
                type:'string',
                width: 20
            },{
                caption:'SDT',
                type:'string',
                width: 20
            },{
                caption:'Chủ phòng máy',
                type:'string',
                width: 40
            },{
                caption:'Ngày tham gia',
                 type:'string',
                 width: 20
            }];

            res.set('Content-Type', 'application/vnd.openxmlformats');
            res.set('Content-Disposition', 'attachment; filename=' + slugName + '.xlsx') ;
            var base64Arr = new Buffer(nodeExcel.execute(conf), 'binary'); // cols & rows are utf-8
            res.send(base64Arr);
          // })
        }
      });
}
