var router = require('express').Router();
var Permission = require('../models/permission');
var crypto = require('crypto');
var AUTH_TOKEN = require('../middleware/authAccessToken');
var PERMISSION = require('../middleware/permission');

router.get('/', AUTH_TOKEN, PERMISSION('GET_PERMISSION'), GET_PERMISSION);
router.post('/', AUTH_TOKEN, PERMISSION('ADD_PERMISSION'), ADD_PERMISSION);
router.put('/', AUTH_TOKEN, PERMISSION('EDIT_PERMISSION'), EDIT_PERMISSION);

module.exports = router;

function GET_PERMISSION(req, res, next) {
  Permission
    .find()
    .exec()
    .then(function(type) {
      if (!type) {
        return res.status(404).json('NOT FOUND');
      } else {
        return res.status(200).json(type)
      }
    })
}

function ADD_PERMISSION(req, res, next) {
  var permission = {
    name: req.body.name,
    description: req.body.description
  }

  Permission
    .create(permission, function(err, resp) {
       if (err) return res.status(400).json(err);
      return res.status(200).json(resp);
    })
}

function EDIT_PERMISSION(req, res, next) {
  var permission = {
    name: req.body.name,
    description: req.body.description
  }

  Permission
    .create(permission, function(err, resp) {
      if (err) return res.status(400).json(err);
      return res.status(200).json(resp);
    })
}
