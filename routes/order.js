var router = require('express').Router();
var Order = require('../models/order');
var User = require('../models/user');
var Type = require('../models/type');
var Mailbox = require('../models/mailbox');
var crypto = require('crypto');
var jwt = require('jsonwebtoken');
var AUTH_TOKEN = require('../middleware/authAccessToken');
var PERMISSION = require('../middleware/permission');
var PAY_CREDIT = require('../middleware/payCredit');
var CONFIG = require('../config');
var CONSTANTS = require('../constants');
var LOG = require('../middleware/log');
var Promise = require('bluebird')
var _ = require('lodash');
var moment = require('moment');

// router.get('/all', AUTH_TOKEN, PERMISSION('GET_ALL'), GET_ALL);
router.get('/', AUTH_TOKEN, PERMISSION('GET_ALL_ORDER'), GET_ALL_ORDER);
router.get('/:id', AUTH_TOKEN, PERMISSION('GET_ORDER'), GET);
router.post('/', AUTH_TOKEN, PERMISSION('ADD_ORDER'), ADD);
router.put('/:id', AUTH_TOKEN, PERMISSION('EDIT_ORDER'), EDIT);
router.delete('/:id', AUTH_TOKEN, PERMISSION('DELETE_ORDER'), DELETE);

module.exports = router;

const userInfo$ = (user, io) => {
  // console.log(user);
  User.findOne({_id: user})
  .exec()
  .then((user) => {
    var userJSON = user.toJSON();
    // console.log(userJSON);
    Type
      .findOne({
        _id: userJSON.type
      })
      .exec()
      .then(function(type) {
        userJSON.type = type.name;
        // console.log(userJSON);
        var userDataToken = _.omit(userJSON, ['password', 'loggedIP']);
        // console.log(userDataToken);
        var token = jwt.sign(userDataToken, CONFIG.SECRET);
        io.to('user/' + userJSON._id).emit('userData', token);
      });
  })
}

const getInbox$ = (id, io) => {
  Mailbox
    .find({ 'receiver.id': id, 'receiver.action.deleted': false })
    .populate({
      path: 'sender.id',
      select: 'username',
      models: 'User'
    })
    .sort({'ctime' : -1})
    .exec()
    .then(function(mails) {
      if (!mails) {
        return res.status(404).json('NOT FOUND');
      } else {
        io.to('user/' + id).emit('inbox', mails);
      }
    })
}

function GET_ALL(req, res, next) {
  Order.find()
    .sort('-ctime')
    .exec()
    .then(r     => res.status(200).json(r))
    .catch(err  => res.status(400).json(err))
}

function GET_ALL_ORDER(req, res, next) {
  var query;
  if(req.user.type === 'admin') {
    query = {};
  } else {
    query = { customerId: req.user._id };
  }
  Order.find(query)
    .populate({
      path: 'customerId',
      select: 'username',
      models: 'User'
    })
    .populate({
      path: 'products.pid',
      models: 'Gift'
    })
    .sort('-ctime')
    .exec()
    .then(r     => res.status(200).json(r))
    .catch(err  => res.status(400).json(err))
}

function GET(req, res, next) {
  var query;
  if(req.user.type === 'admin') {
    query = {_id: req.params.id};
  } else {
    query = {_id: req.params.id, customerId: req.user._id};
  }
  Order.findOne(query)
    .populate({
      path: 'customerId',
      select: 'username',
      models: 'User'
    })
    .populate({
      path: 'products.pid',
      models: 'Gift'
    })
    .exec()
    .then((r)     => {
      if(!r) return res.status(404).json({message: 'Không tìm thấy dữ liệu'});
      return res.status(200).json(r);
    })
    .catch(err  => res.status(400).json(err))
}

function ADD(req, res, next) {
  if(!req.body.order || (req.body.order && !req.body.order.length)) return res.status(404).json({message: 'Vui lòng nhập đầy đủ thông tin'});
  var products = req.body.order;
  var totalOrder = _.reduce(products, (total, product) => {
    return total + parseInt(product.total);
  }, 0);

  if (totalOrder > req.user.credit) return res.status(404).json({message: 'Không đủ xu'});

  var info = {
    orderId: `VP-${moment().unix()}`,
    customerId: req.user._id,
    total: totalOrder,
    products: [],
    history: [
      {
        status: 'NEW',
        note: 'Đơn hàng mới',
        ctime: moment()
      }
    ]
  }

  _.forEach(products, product => {
    var p = {
      pid: product._id,
      quantity: product.quantity,
      total: product.total
    };
    info.products.push(p);
  });

  Order.create(info, (err,r) => {
    if(err) {
      console.log (err)
    } else {
      console.log(info);
      PAY_CREDIT.PAY(req.user._id, parseInt(totalOrder), `Thanh toán xu đổi quà: ${totalOrder} | Mã đơn hàng: #${r.orderId}`);
      userInfo$(req.user._id, req.io);
      var info = {
        title: `Thông báo trạng thái đơn hàng: #${r.orderId} [${CONSTANTS.ORDER_STATUS.NEW}]`,
        description: `Chúng tôi đã cập nhật trạng thái đơn hàng: #${r.orderId}: [${CONSTANTS.ORDER_STATUS.NEW}] \n Vui lòng xem lịch sử đơn hàng để biết thêm chi tiết!`,
        sender: {
          id: req.user._id,
          action: {
            read: true,
            deleted: true
          }
        },
        receiver: {
          id: r.customerId,
          action: {
            read: false,
            deleted: false
          }
        }
      };

      Mailbox
        .create(info, (err, resp) => {
          if (err) {
            return res.status(401).json({ message: err});
          } else {
            getInbox$(r.customerId, req.io);
            return res.status(200).json({message: 'Thành công'});
          }
        })
    }
  })
}

function EDIT(req, res, next) {
  var note;
  if (!req.body.status) {
    return res.status(401).json({message: 'Vui lòng điền dủ thông tin'})
  }

  if(!req.body.note) {
    note = ' ';
  } else {
    note = req.body.note
  }
  var history = {
    status: req.body.status,
    note: note,
    ctime: moment()
  };

  Order
  .findOne({_id: req.params.id})
  .exec()
  .then(o => {
    if(!o) {
      return res.status(404).json({message: 'Không tìm thấy đơn hàng'});
    } else {
      var info = {
        title: `Thông báo trạng thái đơn hàng: #${o.orderId} [${CONSTANTS.ORDER_STATUS[history.status]}]`,
        description: `Chúng tôi đã cập nhật trạng thái đơn hàng: #${o.orderId}: [${CONSTANTS.ORDER_STATUS[history.status]}] \n ${note}`,
        sender: {
          id: req.user._id,
          action: {
            read: true,
            deleted: true
          }
        },
        receiver: {
          id: o.customerId,
          action: {
            read: false,
            deleted: false
          }
        }
      };

      o.history.push(history);
      o.status = history.status;
      o.save();
      Mailbox
        .create(info, (err, resp) => {
          if (err) {
            return res.status(401).json({ message: err});
          } else {
            if (history.status) {
              User.findOne({_id: o.customerId})
              .exec()
              .then(mm => {
                if(mm) {
                  SMS.SEND(mm.phone, 'ORDER_STATUS_SUCCESS');
                }
              })
            }
            LOG.ADD('ORDER_STATUS', o.customerId, `Trạng thái đơn hàng #${o.orderId} đã thay đổi: [${CONSTANTS.ORDER_STATUS[history.status]}]`);
            getInbox$(o.customerId, req.io);
            res.status(200).json({message: 'UPDATE SUCCESS'});
          }
        })
    }

  })
  .catch(err => console.log(err));
  //
  // Order.findByIdAndUpdate(req.params.id, { status: history.status, $push: { history: history } })
  //   .exec()
  //   .then(r     => {
  //     res.status(200).json({message: 'UPDATE SUCCESS'});
  //
  //   })
  //   .catch(err  => res.status(400).json(err))
}

function DELETE(req, res, next) {
  Order.findByIdAndRemove(req.params.id)
    .exec()
    .then(r     => res.status(200).json({message: 'DELETE SUCCESS'}))
    .catch(err  => res.status(400).json(err))
}
