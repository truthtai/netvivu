var router = require('express').Router(); 
var Category = require('../models/category');

router.get('/', GET_CATEGORY);

module.exports = router;

function GET_CATEGORY(req, res) {
	Category
		.find()
		.exec()
		.then(function(category) {
			return res.status(200).json(category);
		})
}
