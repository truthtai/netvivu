var router = require('express').Router();
var Log = require('../models/log');
var User = require('../models/user');
var crypto = require('crypto');
var AUTH_TOKEN = require('../middleware/authAccessToken');
var PERMISSION = require('../middleware/permission');
var mongoose = require('mongoose');
var _ = require('lodash');

router.get('/', AUTH_TOKEN, PERMISSION('GET_ALL_LOGS'), GET_ALL_LOGS);
router.get('/:id', AUTH_TOKEN, PERMISSION('GET_LOGS_USER'), GET_LOGS_USER);
router.get('/user/:id', AUTH_TOKEN, PERMISSION('GET_LOGS_BY_USER'), GET_LOGS_BY_USER);
module.exports = router;

function GET_ALL_LOGS(req, res, next) {
  Log
    .find()
    .sort({'ctime' : -1})
    .limit(100)
    .populate({
      path: 'userId',
      select: '_id name email username',
      models: 'User',
      match: {'_id':{$exists:true}}
    })
    .exec()
    .then(function(logs) {
      if (!logs) {
        return res.status(404).json('NOT FOUND');
      } else {
        return res.status(200).json(logs)
      }
    })
}

function GET_LOGS_BY_USER(req, res, next) {
  User
    .findOne({username: req.params.id})
    .sort({'ctime' : -1})
    .exec()
    .then(x => {
      if(!x) {
        return res.status(404).json('NOT FOUND');
      } else {
        Log
          .find({ userId: x._id })
          .populate({
            path: 'userId',
            select: '_id name email username',
            models: 'User',
            match: {'_id':{$exists:true}}
          })
          .exec()
          .then(function(logs) {
            if (!logs) {
              return res.status(404).json('NOT FOUND');
            } else {
              return res.status(200).json(logs)
            }
          })
      }
    })
}

function GET_LOGS_USER(req, res, next) {
  Log
    .find({ userId: req.params.id })
    .populate('userId', '_id name email')
    .exec()
    .then(function(logs) {
      if (!logs) {
        return res.status(404).json('NOT FOUND');
      } else {
        return res.status(200).json(logs)
      }
    })
}
