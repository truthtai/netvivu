var router = require('express').Router();
var Type = require('../models/type');
var crypto = require('crypto');
var AUTH_TOKEN = require('../middleware/authAccessToken');
var PERMISSION = require('../middleware/permission');
var mongoose = require('mongoose');
var _ = require('lodash');

router.get('/', AUTH_TOKEN, PERMISSION('GET_ALL_TYPE'), GET_ALL_TYPE);
router.post('/', AUTH_TOKEN, PERMISSION('ADD_TYPE'), ADD_TYPE);
router.put('/:id', AUTH_TOKEN, PERMISSION('EDIT_TYPE'), EDIT_TYPE);

module.exports = router;

function GET_ALL_TYPE(req, res, next) {
  Type
    .find()
    //.populate('permission')
    .populate({ 
	     path: 'permission',
	     // populate: {
	     //   path: 'components',
	     //   model: 'Component'
	     // } 
	  })
    .exec()
    .then(function(type) {
      if (!type) {
        return res.status(404).json('NOT FOUND');
      } else {
        return res.status(200).json(type)
      }
    })
}

function ADD_TYPE(req, res, next) {
  var type = {
    name: req.body.name,
    permission: req.body.permission
  }
  // console.log('data >> ' + type);

  Type
    .create(type, function(err, resp) {
      if (err) return res.status(400).json(err);
      return res.status(200).json(resp);
    })
}

function EDIT_TYPE(req, res, next) {
	var pers;
  var type = {
    name: req.body.name,
    permission: req.body.permission
  }
  type.permission = _.uniq(req.body.permission);
  // console.log('data >> ' + type);
  if(type.permission.length) {
  	pers = _.map(type.permission, function(per){
  		return mongoose.Types.ObjectId(per);
  	});
  	type.permission = pers;
  }

  Type
    .update({_id: req.params.id}, type, function(err, resp) {
      if (err) return res.status(400).json(err);
      return res.status(200).json({message: 'EDIT SUCCESS'});
    })
}
