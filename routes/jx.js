var router = require('express').Router();
var User = require('../models/user');
var Type = require('../models/type');
var Log = require('../models/log');
var Promise = require('bluebird');
var moment = require('moment');
var crypto = require('crypto');
var sql = require('mssql');

var AUTH_TOKEN = require('../middleware/authAccessToken');
var PERMISSION = require('../middleware/permission');
var CREDIT = require('../middleware/credit');
var LOG = require('../middleware/log');
var SMS = require('../middleware/sms');
var PAY_CREDIT = require('../middleware/payCredit');

var _ = require('lodash');
var jwt = require('jsonwebtoken');
var CONFIG = require('../config');
var configVLTK = {
  user: 'sa1',
  password: 'a1!',
  server: '117.2.130.12',
  database: 'account_tong',
  options: {},
  parseJSON: true
}

router.get('/', AUTH_TOKEN, PERMISSION('GET_JX'), JX);
router.get('/all', AUTH_TOKEN, PERMISSION('GET_JX_ALL'), JX_ALL);
router.post('/', AUTH_TOKEN, PERMISSION('JOIN_JX'), JOIN_JX);
router.post('/knb', AUTH_TOKEN, PERMISSION('TRADE_KNB_JX'), TRADE_KNB_JX);
module.exports = router;

const userInfo$ = (user, io) => {
  // console.log(user);
  User.findOne({_id: user})
  .exec()
  .then((user) => {
    var userJSON = user.toJSON();
    // console.log(userJSON);
    Type
      .findOne({
        _id: userJSON.type
      })
      .exec()
      .then(function(type) {
        userJSON.type = type.name;
        // console.log(userJSON);
        var userDataToken = _.omit(userJSON, ['password', 'loggedIP']);
        // console.log(userDataToken);
        var token = jwt.sign(userDataToken, CONFIG.SECRET);
        io.to('user/' + userJSON._id).emit('userData', token);
      });
  })
}

function JX(req, res, next) {
  var request = new sql.Request();
  sql.connect(configVLTK).then(function () {
    var q = "select * from account_info where cAccName='" + req.user.username + "'";
    request.query(q, function (err, recordset) {
      if(err) return res.status(404).json({ message: 'NOT FOUND'});
      if(recordset[0]) {
        var infoReturn = {
          username: recordset[0].cAccName,
          knb: recordset[0].nExtPoint1
        }
        return res.status(200).json(infoReturn);
      } else {
        return res.status(404).json({ message: 'NOT FOUND'});
      }
    });
  })
}

function JX_ALL(req, res, next) {
  sql.connect(configVLTK).then((err) => {
    var request = new sql.Request();
    request.query('select * from account_info', function (err, recordset) {
      if(err) return res.status(404).json({ message: err});
      return res.status(200).json(recordset);
    });
  })
  .catch(err => console.log(err))
}

function JOIN_JX(req, res, next) {
  if (!req.body.password) return res.status(400).json({
    message: 'Vui lòng điền đủ thông tin'
  });
  if (req.user.type !== 'admin' && req.user.type !== 'HV' && req.user.type !== 'member') return res.status(400).json({
    message: 'Chỉ thành viên và hội viên mới được tham gia'
  });

  var expTime = moment().add(1825, 'days').format('L');
  var md5Password = crypto.createHash('md5').update(req.body.password).digest("hex");

  var info = {
    username: req.user.username,
    password: md5Password.toUpperCase(),
    secPassword: md5Password.toUpperCase(),
    knb: 0,
    phone: req.user.phone,
    email: req.user.email,
    realName: req.user.name,
    question: 'con heo',
    answer: 'la con heo'
  }

  const queryCreate = (tb) => `INSERT INTO ${tb}(cAccName,cPassWord,cSecPassWord,nExtPoint,nExtPoint1,nExtPoint2,nExtPoint3,nExtPoint4,nExtPoint5,nExtPoint6,nExtPoint7,cFone,cEMail,cRealName,cQuestion,cAnswer) VALUES ('${info.username}', '${info.password}', '${info.secPassword}','1', '${info.knb}','1','1','1','1','1','1','${info.phone}','${info.email}','${info.realName}','${info.question}','${info.answer}')`
  const queryCreate2 = (tb) => `INSERT INTO ${tb}(cAccName,iLeftSecond,dEndDate,iUseSecond) VALUES ('${info.username}','1','${expTime}','1')`;
  console.log(queryCreate2);
  User.findById(req.user._id)
    .exec()
    .then(user => {
      if (user && !user.connected.vltk) {

        sql.connect(configVLTK).then((err) => {
          var request = new sql.Request();
          request.query(queryCreate('Account_Info'), (err1, recordset1) => {
            if (err1) {
              return res.status(400).json({ message: err1.message });
            } else {
              console.log('>>> created a1');
              request.query(queryCreate('Account_Info2'), (err2, recordset2) => {
                if (err2) {
                  console.log(err2);
                } else {
                  console.log('>>> created a2');
                  request.query(queryCreate('Account_Info_BK'), (err3, recordset3) => {
                    if (err3) {
                      console.log(err3);
                    } else {
                      console.log('>>> created a3');
                      request.query(queryCreate2('Account_Habitus'), (err4, recordset4) => {
                        if (err4) {
                          console.log('>>> err a4');
                          console.log(err4);
                          return res.status(400).json({ message: err4.message });
                        } else {
                          console.log('>>> created b1');
                          request.query(queryCreate2('Account_Habitus2'), (err5, recordset5) => {
                            if (err5) {
                              console.log('>>> err a5');
                              console.log(err5);
                            } else {
                              console.log('>>> created b2');
                              request.query(queryCreate2('Account_Habitus_BK'), (err6, recordset6) => {
                                if (err6) {
                                  console.log('>>> err a6');
                                  console.log(err6);
                                } else {
                                  console.log('>>> created b3');
                                  user.connected.vltk = true;
                                  user.save();
                                  userInfo$(user._id, req.io);
                                  return res.status(200).json({ message: 'JOIN SUCCESS'});
                                }
                              });
                            }
                          });
                        }
                      });
                    }
                  });
                }
              });
            }
          });
        })
      } else {
        return res.status(400).json({ message: 'NOT FOUND' });
      }
    })
}

function TRADE_KNB_JX(req, res, next) {
  var request = new sql.Request();
  if (!req.body.knb) return res.status(400).json({
    message: 'Vui lòng điền đủ thông tin'
  });
  req.body.knb = parseInt(req.body.knb);

  User.findById(req.user._id)
    .exec()
    .then(user => {
      if (user) {
        if(!user.connected && !user.connected.vltk) {
          return res.status(400).json({ message: 'Vui lòng tham gia Game để sử dụng tính năng' });
        };
        var xu = req.body.knb * 20;
        if(user.credit < xu) {
          return res.status(400).json({ message: 'Không đủ xu để đổi KNB' });
        } else {
          sql.connect(configVLTK).then(() => {
            var request = new sql.Request();
            request.query(`SELECT cAccName,nExtPoint1,nExtPoint2,cRealName FROM Account_Info WHERE cAccName='${user.username}'`, (errx,r) => {
              if(r) {
                // console.log(r[0].nExtPoint1);
                // console.log(req.body.knb);
                var KNB_CAN_ADD = req.body.knb + parseInt(r[0].nExtPoint1);
                // console.log(KNB_CAN_ADD);
                request.query(`update account_info SET nExtPoint1='${KNB_CAN_ADD}' WHERE cAccName='${user.username}'`, (err, recordset1) => {
                  if (err) {
                    console.log('> ERR 1');
                    return res.status(400).json({ message: err.message});
                  } else {
                    request.query(`update account_info2 SET nExtPoint1='${KNB_CAN_ADD}' WHERE cAccName='${user.username}'`, (err2, recordset2) => {
                      if (err2) {
                        console.log('> ERR 2');
                        return res.status(400).json({ message: err2.message});
                      } else {
                        request.query(`update account_info_bk SET nExtPoint1='${KNB_CAN_ADD}' WHERE cAccName='${user.username}'`, (err3, recordset3) => {
                          if (err3) {
                            console.log('> ERR 3');
                            return res.status(400).json({ message: err3.message});
                          } else {
                            PAY_CREDIT.PAY(user._id, parseInt(xu), `Đổi Xu thành Kim nguyên bảo trong Game: Võ lâm tình duyên: ${req.body.knb} KNB (${xu} xu)`);
                            userInfo$(user._id, req.io);
                            return res.status(200).json({ message: 'TRADE SUCCESS'});
                          }
                        });
                      }
                    });
                  }
                });
              }
            });
          })
        }
      } else {
        return res.status(400).json({ message: 'NOT FOUND' });
      }
    })
}
