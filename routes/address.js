var router = require('express').Router();
var slug = require('slug');
var request = require('request-promise');
var _ = require('lodash');
var fs = require('fs');
var readline = require('readline');
var path = require('path');
var Promise = require('bluebird');
var cities = require('../import/city.json');
var slug = require('slug');
var async = require('async');
var superagentPromisePlugin = require('superagent-promise-plugin');

// router.get('/', GET_ALL);
// router.get('/city', getCity);
router.get('/city', GET_ALL_CITY);
router.get('/city/:id', GET_ALL_PROVINCE);
module.exports = router;

function GET_ALL(req, res, next) {

  var a = _.forEach(cities, function(city) {
    return getDist(city.cID)
    .then(function(x) {
      city.DISTRICTS = JSON.parse(x);
      return city.DISTRICTS
    })
    .then(function(x) {
      // return city
      var o = _.forEach(x, function(dis) {
        dis.dID = dis.CityId;
        dis.name = dis.CityName;
        delete dis['CityId'];
        delete dis['CityName'];
        return dis;
         // return getStreet(dis.CityId).then(function(s) { dis.DISTRICTS = s })
      });
      Promise.all(o).then(function() { return city });
    })
  });

  Promise.all(a).then(function() {
    return res.status(200).json(a);
    // return aaa;
  });
}

function getDist(cID) {
  return new Promise(function(resolve) {
    request.get('http://thukyluat.vn/GiaDat/LoadCity?did=' + cID)
      .then(function(r1) {
        resolve(r1);
      })
  })
}

function getStreet(dID) {
  return new Promise(function(resolve) {
    request.get('http://thukyluat.vn/GiaDat/LoadStreet?did=' + dID)
      .then(function(street) {
        // console.log(street);
        resolve(street);
      });

  })
}

function GET_ALL_CITY(req, res, next) {
  var cityz = _.map(cities, function(city) {
    return _.omit(city, ['DISTRICTS']);
  });
  return res.status(200).json(cityz);
}

function GET_ALL_PROVINCE(req, res, next) {
  var cityz = _.filter(cities, function(city) {
    return city.cID === req.params.id
  });
  return res.status(200).json(cityz[0]);
}

function getCity(req, res, next) {
  var aaa = [];
  readline.createInterface({
    input: fs.createReadStream(path.join('city.txt')),
    terminal: false
  }).on('line', function(line) {
    var id = line.split(' ')[0];
    var slugCity = slug(line.replace(id + ' ', '').toUpperCase(), '_')
    var ax = {};
    ax['cID'] = id;
    ax['name'] = line.replace(id + ' ', '');
    aaa.push(ax);
  }).on('close', function() {
    return res.status(200).json(aaa);
    // return aaa;
  });
}
