var router = require('express').Router();
var Mailbox = require('../models/mailbox');
var User = require('../models/user');
var crypto = require('crypto');
var Type = require('../models/type');
var AUTH_TOKEN = require('../middleware/authAccessToken');
var PERMISSION = require('../middleware/permission');
var PAY_CREDIT = require('../middleware/payCredit');
var LOG = require('../middleware/log');
var mongoose = require('mongoose');
var _ = require('lodash');
var jwt = require('jsonwebtoken');
var CONFIG = require('../config');

router.get('/all', AUTH_TOKEN, PERMISSION('GET_ALL_MAILBOX'), GET_ALL_MAILBOX);
router.get('/inbox', AUTH_TOKEN, PERMISSION('GET_ALL_INBOX_MAILBOX'), GET_ALL_INBOX_MAILBOX);
router.get('/outbox', AUTH_TOKEN, PERMISSION('GET_ALL_OUTBOX_MAILBOX'), GET_ALL_OUTBOX_MAILBOX);
router.get('/:id/receive', AUTH_TOKEN, PERMISSION('RECEIVE_CREDIT_MAILBOX'), RECEIVE_CREDIT_MAILBOX);
router.get('/:id', AUTH_TOKEN, PERMISSION('GET_MAILBOX'), GET_MAILBOX);
router.delete('/:id', AUTH_TOKEN, PERMISSION('DELETE_MAILBOX'), DELETE_MAILBOX);
router.post('/', AUTH_TOKEN, PERMISSION('SEND_MAILBOX'), SEND_MAILBOX);

module.exports = router;

const userInfo$ = (user, io) => {
  // console.log(user);
  User.findOne({_id: user})
  .exec()
  .then((user) => {
    var userJSON = user.toJSON();
    // console.log(userJSON);
    Type
      .findOne({
        _id: userJSON.type
      })
      .exec()
      .then(function(type) {
        userJSON.type = type.name;
        // console.log(userJSON);
        var userDataToken = _.omit(userJSON, ['password', 'loggedIP']);
        // console.log(userDataToken);
        var token = jwt.sign(userDataToken, CONFIG.SECRET);
        io.to('user/' + userJSON._id).emit('userData', token);
      });
  })
}


const getInbox$ = (id, io) => {
  Mailbox
    .find({ 'receiver.id': id, 'receiver.action.deleted': false })
    .populate({
      path: 'sender.id',
      select: 'username',
      models: 'User'
    })
    .sort({'ctime' : -1})
    .exec()
    .then(function(mails) {
      if (!mails) {
        return res.status(404).json('NOT FOUND');
      } else {
        io.to('user/' + id).emit('inbox', mails);
      }
    })
}

const getOutbox$ = (id, io) => {
  Mailbox
    .find({ 'sender.id': id, 'sender.action.deleted': false })
    .populate({
      path: 'receiver.id',
      select: 'username',
      models: 'User'
    })
    .sort({'ctime' : -1})
    .exec()
    .then(function(mails) {
      if (!mails) {
        return res.status(404).json('NOT FOUND');
      } else {
        io.to('user/' + id).emit('outbox', mails);
      }
    })
}

const getInboxUnread$ = (id, io) => {
  Mailbox
    .find({ 'receiver.id': id, 'receiver.action.deleted': false, 'receiver.action.read': false })
    .populate({
      path: 'sender.id',
      select: 'username',
      models: 'User'
    })
    .sort({'ctime' : -1})
    .exec()
    .then(function(mails) {
      if (!mails) {
        return res.status(404).json('NOT FOUND');
      } else {
        io.to('user/' + id).emit('inboxUnread', mails);
      }
    })
}

function GET_ALL_MAILBOX(req, res, next) {
  Mailbox
    .find()
    .populate({
      path: 'sender.id',
      select: 'username',
      models: 'User'
    })
    .populate({
      path: 'receiver.id',
      select: 'username',
      models: 'User'
    })
    .sort({'ctime' : -1})
    .exec()
    .then(function(mails) {
      if (!mails) {
        return res.status(404).json('NOT FOUND');
      } else {
        return res.status(200).json(mails)
      }
    })
}

function GET_ALL_INBOX_MAILBOX(req, res, next) {
  Mailbox
    .find({ 'receiver.id': req.user._id, 'receiver.action.deleted': false })
    .populate({
      path: 'sender.id',
      select: 'username',
      models: 'User'
    })
    .sort({'ctime' : -1})
    .exec()
    .then(function(mails) {
      if (!mails) {
        return res.status(404).json('NOT FOUND');
      } else {
        return res.status(200).json(mails)
      }
    })
}

function GET_ALL_OUTBOX_MAILBOX(req, res, next) {
  Mailbox
    .find({ 'sender.id': req.user._id, 'sender.action.deleted': false })
    .populate({
      path: 'receiver.id',
      select: 'username',
      models: 'User'
    })
    .sort({'ctime' : -1})
    .exec()
    .then(function(mails) {
      if (!mails) {
        return res.status(404).json('NOT FOUND');
      } else {
        return res.status(200).json(mails)
      }
    })
}

function GET_MAILBOX(req, res, next) {
  Mailbox
    .findOne({ _id: req.params.id, $or: [ { 'sender.id': req.user._id }, { 'receiver.id': req.user._id }] })
    .populate({
      path: 'sender.id',
      select: 'username',
      models: 'User'
    })
    .populate({
      path: 'receiver.id',
      select: 'username',
      models: 'User'
    })
    .exec()
    .then(mail => {
      if (!mail) {
        return res.status(404).json('Không tìm thấy thư nào');
      } else {
        var currentUser;
        if(String(req.user._id) === String(mail.sender.id._id)) {
          currentUser = 'sender';
        } else {
          currentUser = 'receiver';
        }

        mail[currentUser]['action']['read'] = true;

        if(String(req.user._id) === String(mail.receiver.id._id) && String(req.user._id) === String(mail.receiver.id._id)) {
          mail['receiver']['action']['read'] = true;
        }

        mail.save();
        getInbox$(req.user._id, req.io);
        // mail.description = mail.description.replace('\n', '<br>');
        return res.status(200).json(mail);
      }
    })
}

function DELETE_MAILBOX(req, res, next) {
  Mailbox
    .findById(req.params.id)
    .exec()
    .then(mail => {
      if (!mail) {
        return res.status(404).json('Không tìm thấy thư nào');
      } else {
        var currentUser;
        if(String(req.user._id) === String(mail.sender.id)) {
          currentUser = 'sender';
        } else {
          currentUser = 'receiver';
        }

        mail[currentUser]['action']['deleted'] = true;
        if(String(req.user._id) === String(mail.receiver.id) && String(req.user._id) === String(mail.receiver.id)) {
          mail['receiver']['action']['deleted'] = true;
        }
        mail.save();
        getInbox$(req.user._id, req.io);
        getOutbox$(req.user._id, req.io);
        return res.status(200).json(mail);
      }
    })
}

function RECEIVE_CREDIT_MAILBOX(req, res, next) {
  Mailbox
    .findById(req.params.id)
    .populate({
      path: 'sender.id',
      select: 'username',
      models: 'User'
    })
    .populate({
      path: 'receiver.id',
      select: 'username',
      models: 'User'
    })
    .exec()
    .then(mail => {
      if (!mail) {
        return res.status(404).json('Không tìm thấy thư nào');
      } else {
        var currentUser;
        var anotherUser;
        if(String(req.user._id) === String(mail.sender.id)) {
          currentUser = 'sender';
          anotherUser = 'receiver';
        } else {
          currentUser = 'receiver';
          anotherUser = 'sender';
        }

        if(currentUser === 'receiver') {
          if(mail[currentUser]['action']['confirmCredit']) {
            return res.status(401).json('Bạn đã giao dịch rồi');
          }

          if(mail[anotherUser].credit && mail[anotherUser].credit > 0) {
            PAY_CREDIT.ADD(mail[currentUser].id, mail[anotherUser].credit, `Giao dịch Nhận xu: ${mail[anotherUser].credit} | ${mail.title} | Người gửi: ${mail[anotherUser].id.username}`);
            LOG.ADD('RECEIVE_CREDIT_MAILBOX', mail[currentUser].id, `Giao dịch Nhận xu thành công: ${mail[anotherUser].credit}`);
            LOG.ADD('RECEIVE_CREDIT_MAILBOX', mail[anotherUser].id, `${mail[currentUser].id.username} đã nhận Giao dịch xu: ${mail[anotherUser].credit}`);
            mail[currentUser]['action']['confirmCredit'] = true;
          }
          mail.save();
          getInbox$(req.user._id, req.io);
          getOutbox$(req.user._id, req.io);
          userInfo$(req.user._id, req.io);
          console.log('ok');
          return res.status(200).json(mail);
        } else {
          return res.status(401).json('Bạn Không được phép nhận giao dịch này');
        }
      }
    })
}

function SEND_MAILBOX(req, res, next) {
  if(!req.body.title || !req.body.description || !req.body.receiver) {
    return res.status(404).json({ message: 'Vui lòng điền đầy đủ thông tin' });
  }
  var targetUser;
  var currentUser;
  var info = {
    title: req.body.title,
    description: req.body.description,
    sender: {
      id: req.user._id,
      action: {
        read: false,
        deleted: false
      }
    },
    receiver: {
      id: req.body.receiver,
      action: {
        read: false,
        deleted: false
      }
    }
  }

  if(req.body.credit && req.body.credit > req.user.credit) {
    return res.status(401).json({ message: 'Không đủ xu để thực hiện'});
  }

  if(req.body.credit && req.body.credit <= req.user.credit) {
    User.findById(req.body.receiver)
    .exec()
    .then(u => {
      if(!u) {
        return res.status(401).json({ message: 'Không tìm thấy người nhận'});
      } else {
        targetUser = u.toJSON();
        User.findById(req.user._id)
        .exec()
        .then(ux => {
          if(!ux) {
            return res.status(401).json({ message: 'Không tìm thấy người nhận'});
          } else {
            currentUser = ux.toJSON();
            // console.log(targetUser.ownerId);
            // console.log(currentUser._id);
            //
            // console.log(currentUser.ownerId);
            // console.log(targetUser._id);

            if ((String(targetUser.ownerId) && String(targetUser.ownerId) === String(currentUser._id) ) || (String(currentUser.ownerId) && String(targetUser._id) === String(currentUser.ownerId))) {
                PAY_CREDIT.PAY(req.user._id, parseInt(req.body.credit), `Thanh toán xu cho Gửi thư kèm giao dịch xu: ${req.body.credit} | ${req.body.title} | ID người nhận: ${req.body.receiver}`);
                info.sender.credit = req.body.credit;
                Mailbox
                  .create(info, (err, resp) => {
                    if (err) {
                      return res.status(401).json({ message: err});
                    } else {
                      getInbox$(req.body.receiver, req.io);
                      getOutbox$(req.user._id, req.io);
                      userInfo$(req.user._id, req.io);
                      return res.status(200).json({ message: 'Gửi thư thành công'})
                    }
                  })
            } else {
              return res.status(401).json({ message: 'Chỉ giao dịch xu trong nội bộ phòng máy'});
            }
          }
        });
      }
    })
  } else {
    Mailbox
      .create(info, (err, resp) => {
        if (err) {
          return res.status(401).json({ message: err});
        } else {
          getInbox$(req.body.receiver, req.io);
          getOutbox$(req.user._id, req.io);
          userInfo$(req.user._id, req.io);
          return res.status(200).json({ message: 'Gửi thư thành công'})
        }
      })
  }






  // if(req.body.credit && req.body.credit <= req.user.credit) {
  //   PAY_CREDIT.PAY(req.user._id, parseInt(req.body.credit), `Thanh toán xu cho Gửi thư kèm giao dịch xu: ${req.body.credit} | ${req.body.title} | ID người nhận: ${req.body.receiver}`);
  //   info.sender.credit = req.body.credit;
  // }
}
