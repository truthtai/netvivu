var router = require('express').Router();
var News = require('../models/news');
var crypto = require('crypto');
var AUTH_TOKEN = require('../middleware/authAccessToken');
var PERMISSION = require('../middleware/permission');
var mongoose = require('mongoose');
var _ = require('lodash');
var slug = require('slug');

router.get('/', GET_ALL_NEWS);
router.get('/:id', GET_NEWS);
router.post('/', AUTH_TOKEN, PERMISSION('ADD_NEWS'), ADD_NEWS);
router.put('/:id', AUTH_TOKEN, PERMISSION('EDIT_NEWS'), EDIT_NEWS);
router.delete('/:id', AUTH_TOKEN, PERMISSION('DELETE_NEWS'), DELETE_NEWS);

module.exports = router;

function GET_ALL_NEWS(req, res, next) {
  News
    .find()
    .populate('ownerId', '_id name email')
    .exec()
    .then(function(news) {
      if (!news) {
        return res.status(404).json('NOT FOUND');
      } else {
        return res.status(200).json(news)
      }
    })
}


function GET_NEWS(req, res, next) {
  News
    .findOne({
      slug: req.params.id
    })
    .populate('ownerId', '_id name email')
    .exec()
    .then(function(news) {
      if (!news) {
        return res.status(404).json('NOT FOUND');
      } else {
        return res.status(200).json(news)
      }
    })
}

function ADD_NEWS(req, res, next) {
  var news = {
    name: req.body.name,
    description: req.body.description,
    ownerId: req.user._id,
    tags: ['Tin tức'],
    slug: slug(req.body.name, {
      lower: true
    }) + '-' + new Date().getTime(),
  }

  News
    .create(news, function(err, resp) {
      if (err) return res.status(400).json(err);
      return res.status(200).json(resp);
    })
}

function EDIT_NEWS(req, res, next) {

  var news = {
    name: req.body.name,
    description: req.body.description,
    utime: new Date()
  }

  News
    .update({
      _id: req.params.id
    }, news, function(err, resp) {
      if (err) return res.status(400).json(err);
      return res.status(200).json({
        message: 'EDIT SUCCESS'
      });
    })
}

function DELETE_NEWS(req, res, next) {
  News
    .where().findOneAndRemove({
      _id: req.params.id
    }, function(err, resp) {
      if (err) {
        return res.status(401).json('NOT DELETE');
      } else {
        // t.status =
        return res.status(200).json({
          message: 'DELETED SUCCESS'
        })
      }
    }) // executes
}
