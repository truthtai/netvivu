var router = require('express').Router();
var Setting = require('../models/setting');
var crypto = require('crypto');
var AUTH_TOKEN = require('../middleware/authAccessToken');
var PERMISSION = require('../middleware/permission');

router.get('/', AUTH_TOKEN, PERMISSION('GET_SETTING'), GET_SETTING);
router.post('/', AUTH_TOKEN, PERMISSION('ADD_SETTING'), ADD_SETTING);
router.put('/', AUTH_TOKEN, PERMISSION('EDIT_SETTING'), EDIT_SETTING);

module.exports = router;

function GET_SETTING(req, res, next) {
  Setting
    .find()
    .exec()
    .then(function(s) {
      if (!s) {
        return res.status(404).json('NOT FOUND');
      } else {
        return res.status(200).json(s)
      }
    })
}

function ADD_SETTING(req, res, next) {
  var permission = {
    name: req.body.name,
    value: req.body.value
  }

  Setting
    .create(permission, function(err, resp) {
       if (err) return res.status(400).json(err);
      return res.status(200).json(resp);
    })
}

function EDIT_SETTING(req, res, next) {
  var permission = {
    name: req.body.name,
    value: req.body.value
  }

  Setting
    .create(permission, function(err, resp) {
      if (err) return res.status(400).json(err);
      return res.status(200).json(resp);
    })
}
