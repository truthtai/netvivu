var router = require('express').Router();
var Gift = require('../models/gift');
var crypto = require('crypto');
var AUTH_TOKEN = require('../middleware/authAccessToken');
var PERMISSION = require('../middleware/permission');
var PAYCREDIT = require('../middleware/payCredit');
var UPLOAD = require('../middleware/upload');

router.get('/', AUTH_TOKEN, PERMISSION('GET_ALL_GIFT'), GET_ALL);
router.get('/cat/:id', AUTH_TOKEN, GET_ALL_C);
router.get('/:id', AUTH_TOKEN, PERMISSION('GET_GIFT'), GET);
router.post('/', AUTH_TOKEN, PERMISSION('ADD_GIFT'), UPLOAD, ADD);
router.put('/:id', AUTH_TOKEN, PERMISSION('EDIT_GIFT'), EDIT);
router.delete('/:id', AUTH_TOKEN, PERMISSION('DELETE_GIFT'), DELETE);

module.exports = router;

function GET_ALL(req, res, next) {
  Gift.find().sort('name')
    .exec()
    .then(r     => res.status(200).json(r))
    .catch(err  => res.status(400).json(err))
}

function GET_ALL_C(req, res, next) {

  var cat = req.params.id;
  Gift
    .aggregate([
      { $match: { category: cat, status: true } },
      { $group : { _id: "$subCategory", products: { $push: "$$ROOT" } } }
    ])
    .exec()
    .then(r     => res.status(200).json(r))
    .catch(err  => res.status(400).json(err))
}

function GET(req, res, next) {
  Gift.findOne({_id: req.params.id})
    .exec()
    .then((r)     => {
      if(!r) return res.status(404).json({message: 'NOT FOUND'});
      return res.status(200).json(r);
    })
    .catch(err  => res.status(400).json(err))
}

function ADD(req, res, next) {
  req.body.image = req.files[0].filename;
  Gift.create(req.body)
    .then(r     => res.status(200).json(r))
    .catch(err  => res.status(400).json(err))
}

function EDIT(req, res, next) {
  Gift.findByIdAndUpdate(req.params.id, req.body)
    .exec()
    .then(r     => res.status(200).json({message: 'UPDATE SUCCESS'}))
    .catch(err  => res.status(400).json(err))
}

function DELETE(req, res, next) {
  Gift.findByIdAndRemove(req.params.id)
    .exec()
    .then(r     => res.status(200).json({message: 'DELETE SUCCESS'}))
    .catch(err  => res.status(400).json(err))
}
