var Agenda = require('agenda');
var CONFIG = require('../config');
var app = require('express')();
var agendaUI = require('agenda-ui');

var agenda = new Agenda({ db: { address: CONFIG.DATABASE, collection: 'agendaJobs' } });
app.use('/agenda-ui', agendaUI(agenda, {poll: 1000}));

agenda.processEvery('2 seconds');

var jobs = ['rebackChallenge', 'rebackMailCredit'];

// require('../jobs/removeUser')(agenda);
require('../jobs/task')(agenda);

agenda.on('ready', function() {
	console.log('connected jobs');
	jobs.forEach((type) => {
		agenda.every('2 seconds', type);
	})
  agenda.start();
});

module.exports = agenda;
