var Challenge = require('../models/challenge');
var PAY_CREDIT = require('../middleware/payCredit');
var _ = require('lodash');
var config = {};
var User = require('../models/user');

module.exports = function (io) {
  // Set socket.io listeners.
  io.sockets.on('connection', (socket) => {
    socket.join('connected');
    socket.on('user', (user) => {
      console.log(`connected: ${user._id}`);
      socket.join('user/' + user._id);

      User.findOne({_id: user._id})
      .exec()
      .then(v => {
        if(v && v.status !== 'active') {
          io.to('user/' + user._id).emit('notifSysx', `Tài khoản đã bị khóa. Vui lòng liên hệ quản trị để biết thêm chi tiết`);
          //return res.status(401).json({message: 'Tài khoản đã bị khóa. Vui lòng liên hệ quản trị để biết thêm chi tiết'})
        }
      })

    });

    socket.on('joinChallenge', (data)  => {
      config[data.joinChallenge._id] = {
        countTime: {
          sec: 0,
          min: 0
        }
      }
      console.log('connect to room: ' + data.joinChallenge._id);
      socket.join('challengeRoom/' + data.joinChallenge._id);
      // req.io.in('challengeRoom/' + data.joinChallenge._id).emit('config', config[nameConfig]);
      Challenge.findOne({ _id: data.joinChallenge._id})
        .exec()
        .then(joinChallenge => {
          if(!joinChallenge){
            console.log('NOT FOUND');
          } else {
            io.to('challengeRoom/' + data.joinChallenge._id).emit('config', joinChallenge);
            io.to('challengeRoom/' + data.joinChallenge._id).emit('notif', `${data.user.username} đã vào phòng`);
          }
        }).catch((err) => console.log(err))


      socket.on('disconnect', () => {
        console.log(`disconnect ROOM: ${data.user.username}`);
        socket.leave('challengeRoom/' + data.joinChallenge._id);
        io.to('challengeRoom/' + data.joinChallenge._id).emit('notif', `${data.user.username} đã vào phòng`);
      });
    });

    socket.on('confirm', (data) => {
        Challenge.findOne({ _id: data.joinChallenge._id})
          .exec()
          .then(joinChallenge => {
            if(!joinChallenge){
              console.log('NOT FOUND');
            } else {
              if(String(data.user._id) === String(joinChallenge.teamA.userId)) {
                Challenge.findByIdAndUpdate(data.joinChallenge._id, { teamA: data.joinChallenge.teamA })
                .exec()
                .then(() => {
                  console.log('ok team A');
                })
              } else {
                Challenge.findByIdAndUpdate(data.joinChallenge._id, { teamB: data.joinChallenge.teamB })
                .exec()
                .then(() => {
                  console.log('ok team B');
                })
              }
            }
          }).catch((err) => console.log(err))
      });

    socket.on('getConfig', (data) => {
      Challenge.findOne({ _id: data.joinChallenge._id})
        .exec()
        .then(joinChallenge => {
          if(!joinChallenge){
            console.log('NOT FOUND');
          } else {
            io.to('challengeRoom/' + data.joinChallenge._id).emit('config', joinChallenge);
          }
        }).catch((err) => console.log(err))
    });

    socket.on('setConfig', (data) => {
      var currentTeam;

      Challenge.findOne({ _id: data.joinChallenge._id})
        .exec()
        .then(joinChallenge => {
          if(!joinChallenge){
            // console.log('NOT FOUND');
          } else {
            if(joinChallenge.isWin) {
              io.in('challengeRoom/' + joinChallenge._id).emit('isWin', 'Đã có đội thắng. Vui lòng Chọn đội khác !');
            } else {
              if(String(joinChallenge.teamA.userId) === String(data.user._id))  {
                currentTeam = 'teamA';
              } else {
                currentTeam = 'teamB';
              }
              console.log(currentTeam);
              // console.log(data.config);
              _.forEach(data.config, (v, k) => {
                var nn = `${currentTeam}.${k}`;
                var xc = {[nn]: v};

                console.log(xc);
                Challenge.findByIdAndUpdate(data.joinChallenge._id, {$set: xc})
                .exec()
                .then(() => {
                  console.log('OK update');
                  Challenge.findOne({ _id: data.joinChallenge._id})
                    .exec()
                    .then(challengeUpdated => {
                      // console.log(challengeUpdated);
                      console.log('updated');
                      io.in('challengeRoom/' + data.joinChallenge._id).emit('config', challengeUpdated.toJSON());
                      // console.log('>>> next')

                      console.log(challengeUpdated.teamA.countingTime);
                      var timer;
                      // clearInterval(timer);
                      if(challengeUpdated.teamA.started && challengeUpdated.teamB.started && !challengeUpdated.countingTime) {
                        Challenge.findByIdAndUpdate(data.joinChallenge._id, { countingTime: true })
                        .exec()
                        .then(() => {});

                        var min = 89;
                        var sec = 60;

                        timer = setInterval(() => {
                          var a = new Date();
                          sec --;
                          if(sec === 0) {
                            min--;
                            sec = 60;
                          }
                          config[data.joinChallenge._id].countTime.sec = sec;
                          config[data.joinChallenge._id].countTime.min = min;

                          console.log('Min: ' + min + ' - sec:' + sec);
                          console.log(min === 89);
                          console.log(sec === 0);
                          if (min === 0 && sec === 1) {
                            console.log('>>>>> ok end');
                            clearInterval(timer);
                            Challenge.findByIdAndUpdate(joinChallenge._id, { endTime: { min: 90, sec: 0 }, isWin: null })
                            .exec()
                            .then(() => {
                              console.log('time out  => update');
                              io.in('challengeRoom/' + joinChallenge._id).emit('isWin', 'Đã hết giờ thi đấu , vui lòng gửi thông tin + hình ảnh về vivuplay.vn@gmail.com để được xử lý');
                              // io.in('challengeRoom/' + joinChallenge._id).emit('msg', 'Đã hết giờ thi đấu , vui lòng gửi thông tin + hình ảnh về vivuplay.vn@gmail.com để được xử lý');
                            })
                            .catch((err) => console.log(err));
                          }
                          // console.log(config[data.joinChallenge._id].countTime);
                          io.in('challengeRoom/' + joinChallenge._id).emit('countTimer', config[data.joinChallenge._id].countTime);
                        }, 1000);
                      }


                      const payForWin = (id) => {
                        const award = parseInt(challengeUpdated.credit) + (parseInt(challengeUpdated.credit) - parseInt(challengeUpdated.fee));
                        PAY_CREDIT.ADD(id, award, `Phần thưởng cho đội thắng: ${award} | ${challengeUpdated.name} | ${challengeUpdated._id}`);
                        Challenge.findByIdAndUpdate(data.joinChallenge._id, { isWin: joinChallenge.teamA.userId })
                        .exec()
                        .then(() => console.log('ok'))
                        .catch((err) => console.log(err));
                      }

                      console.log(challengeUpdated.teamA.isWin);
                      console.log(challengeUpdated.teamB.isWin);
                      // CHECK isWin A
                      console.log('>>> check A win');
                      console.log(challengeUpdated.teamA.isWin && !challengeUpdated.teamB.isWin);
                      console.log('>>> check B win');
                      console.log(!challengeUpdated.teamA.isWin && challengeUpdated.teamB.isWin);

                      const setResult = () => {
                        if (challengeUpdated.teamA.isWin && challengeUpdated.teamB.isWin === false) {
                          // timer.stop();

                          console.log('Đã có kết quả: đội A thắng');
                          Challenge.findByIdAndUpdate(data.joinChallenge._id, { endTime: { min: (90 - config[data.joinChallenge._id].countTime.min), sec: (90 - config[data.joinChallenge._id].countTime.sec) }, isWin: challengeUpdated.teamA.userId })
                          .exec()
                          .then(() => {

                            payForWin(challengeUpdated.teamA.userId);
                            // console.log('Đã có kết quả: đội A thắng');
                            io.in('challengeRoom/' + data.joinChallenge._id).emit('isWin', 'Đã có kết quả: đội A thắng');
                            clearInterval(timer);
                          })
                          .catch((err) => console.log(err));
                        }

                        if (challengeUpdated.teamA.isWin === false && challengeUpdated.teamB.isWin) {

                          // console.log('Đã có kết quả: đội B thắng');
                          Challenge.findByIdAndUpdate(data.joinChallenge._id, { endTime: { min: (90 - config[data.joinChallenge._id].countTime.min), sec: (90 - config[data.joinChallenge._id].countTime.sec) }, isWin: challengeUpdated.teamB.userId })
                          .exec()
                          .then(() => {

                            payForWin(challengeUpdated.teamB.userId);
                            console.log('Đã có kết quả: đội B thắng');
                            io.in('challengeRoom/' + data.joinChallenge._id).emit('isWin', 'Đã có kết quả: đội B thắng');
                            clearInterval(timer);
                          })
                          .catch((err) => console.log(err));
                        }
                      };

                      if ((challengeUpdated.teamA.isWin && challengeUpdated.teamB.isWin === false) || (challengeUpdated.teamB.isWin && challengeUpdated.teamA.isWin === false) ) {
                        io.in('challengeRoom/' + data.joinChallenge._id).emit('msg', 'Kết quả sẽ được chấp nhận sau 5 phút nếu không có bất kỳ thay đổi' );
                        setTimeout(() => {
                          setResult();
                        }, 300000)
                      }
                    });
                })
                .catch((err) => console.log(err));
              });
            }

          }
        })
        .catch()


    })

    socket.on('getTeam', (data) => {
      var currentTeam;
      Challenge.findOne({ _id: data.joinChallenge._id})
        .exec()
        .then(joinChallenge => {
          if(!joinChallenge){
            console.log('NOT FOUND');
          } else {
            if(String(joinChallenge.teamA.userId) === String(data.user._id))  {
              currentTeam = 'teamA';
            } else {
              currentTeam = 'teamB';
            }
            console.log(currentTeam);
            // socket.in('challengeRoom/' + data.joinChallenge._id).emit('isTeam', currentTeam);
          }
        })
    });
    socket.on('disconnect', (socket) => {
      console.log(`a user: disconnected`);
    });
  });
}
