var User = require('../models/user.js');
var LOG = require('../middleware/log');
var _ = require('lodash');
var Challenge = require('../models/challenge');
var Mailbox = require('../models/mailbox');
var PAY_CREDIT = require('../middleware/payCredit');
var moment = require('moment');

var currentTime = moment().subtract(30, 'minutes');
var oneDay = moment().subtract(1, 'days');
var currentTimeDate = new Date();
module.exports = function(agenda) {
  agenda.define('rebackChallenge', {lockLifetime: 10000}, function(job, done) {
    // console.log('finding user will remove');
    Mailbox.find({ ctime: { $lte: currentTime }, 'teamB.userId': null, active: true })
      .exec()
      .then(function(res) {
        _.forEach(res, function(r) {
          console.log('reback room ' + r._id);
          Challenge
            .findByIdAndUpdate(r._id, { active: false }, function(err, resp) {
              if (err) {
                console.log(err);
              } else {
                PAY_CREDIT.ADD(r.teamA.userId, r.credit, `Hoàn trả xu vì phòng Thách đấu không có ai tham gia sau 30p: ${r.credit}`);
              }
            })
        });
        done();
      })

  });

  agenda.define('rebackMailCredit', {lockLifetime: 10000}, function(job, done) {
    // console.log('finding user will remove');
    Mailbox.find({ ctime: { $lte: oneDay }, 'receiver.action.confirmCredit': false, 'receiver.credit': { $exits: true } })
      .populate({
        path: 'sender.id',
        select: 'username',
        models: 'User'
      })
      .populate({
        path: 'receiver.id',
        select: 'username',
        models: 'User'
      })
      .exec()
      .then(function(res) {
        _.forEach(res, function(r) {
          console.log('reback room ' + r._id);
          Mailbox
            .findByIdAndUpdate(r.receiver.id._id, { $set: { 'receiver.action.deleted': true } }, function(err, resp) {
              if (err) {
                console.log(err);
              } else {
                PAY_CREDIT.ADD(r.sender.id._id, r.sender.credit, `Hoàn trả xu vì người nhận sau 1 ngày: ${r.sender.credit} | Người nhận: ${r.receiver.id.username}`);
              }
            })
        });
        done();
      })

  });

  // agenda.define('removeUser', {lockLifetime: 10000}, function(job, done) {
  //   // console.log('finding user will remove');
  //   User.findOne({
  //     username: 'truthtai'
  //   }).exec().then(function (u) {
  //     User.find({ willRemoveAt: { $lte: currentTimeDate } })
  //       .exec()
  //       .then(function(res) {
  //         _.forEach(res, function(r) {
  //           console.log('deleteing ' + r._id);
  //           User
  //             .findByIdAndRemove(r._id, function(err, resp) {
  //               if (err) {
  //                 // return res.status(401).json('NOT DELETE');
  //                 LOG.ADD('REMOVE_USER', u._id, err);
  //               } else {
  //                 LOG.ADD('REMOVE_USER', u._id, 'Xóa tài khoản thành công :' + r.username +' | '+ r.phone +' | '+ r.email);
  //               }
  //             })
  //         });
  //         done();
  //       })
  //   })
  //
  // });
}
