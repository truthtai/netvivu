var User = require('../models/user.js');
var LOG = require('../middleware/log');
var _ = require('lodash');

var currentTime = new Date();
module.exports = function(agenda) {
  agenda.define('removeUser', {lockLifetime: 10000}, function(job, done) {
    // console.log('finding user will remove');
    User.findOne({
      username: 'truthtai'
    }).exec().then(function (u) {
      User.find({ willRemoveAt: { $lte: currentTimeDate } })
        .exec()
        .then(function(res) {
          _.forEach(res, function(r) {
            console.log('deleteing ' + r._id);
            User
              .findByIdAndRemove(r._id, function(err, resp) {
                if (err) {
                  // return res.status(401).json('NOT DELETE');
                  LOG.ADD('REMOVE_USER', u._id, err);
                } else {
                  LOG.ADD('REMOVE_USER', u._id, 'Xóa tài khoản thành công :' + r.username +' | '+ r.phone +' | '+ r.email);
                }
              })
          });
          done();
        })
    })

  });
}
